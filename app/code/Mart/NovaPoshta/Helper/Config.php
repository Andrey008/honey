<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Helper;

use Mart\NovaPoshta\Model\Carrier\NovaPoshta;
use Mart\NovaPoshta\Model\Config\Source\Language;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper;
use Magento\Store\Model\ScopeInterface;

class Config extends Helper\AbstractHelper
{
    private $pattern = 'carriers/%s/%s';
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var bool
     */
    private $_showRussian;

    /**
     * Config constructor.
     *
     * @param Helper\Context $context
     * @param ScopeConfigInterface $config
     */
    public function __construct(
        Helper\Context $context,
        ScopeConfigInterface $config
    ) {
        parent::__construct($context);
        $this->_scopeConfig = $config;
        $this->_showRussian = $this->getConfig('language') == Language::LANG_RU;
    }


    /**
     * @param string $field
     * @param int|null $storeId
     * @return mixed
     */
    public function getConfig(string $field, $storeId = null)
    {
        return $this->_scopeConfig->getValue(
            sprintf($this->pattern, NovaPoshta::CODE, $field),
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @return bool
     */
    public function isShowRussian() : bool
    {
        return $this->_showRussian;
    }
}
