<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    protected $customAttributes = [
        'city_id',
        'warehouse_id',
    ];

    /**
     * @return array
     */
    public function getCustomAttributesList()
    {
        return $this->customAttributes;
    }
}
