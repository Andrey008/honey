<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Cron;

use Mart\NovaPoshta\Api\UpdateInterface;

class UpdateData
{
    /**
     * @var UpdateInterface[]
     */
    protected $updateEntities = [];

    /**
     * UpdateData constructor.
     *
     * @param array $updateEntities
     */
    public function __construct(array $updateEntities = []) {
        $this->updateEntities = $updateEntities;
    }

    /**
     * @return void
     */
    public function execute()
    {
        foreach ($this->updateEntities as $entity) {
            if (! $entity instanceof UpdateInterface)
                continue;
            $entity->updateAll();
        }
    }
}
