<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Api;

/**
 * Interface AddressManagementInterface
 * @package Mart\NovaPoshta\Api
 */
interface AddressManagementInterface extends UpdateInterface
{
    /**
     * @return boolean
     */
    public function updateAreas();

    /**
     * @return boolean
     */
    public function updateCities();

    /**
     * @return boolean
     */
    public function updateWarehouses();
}
