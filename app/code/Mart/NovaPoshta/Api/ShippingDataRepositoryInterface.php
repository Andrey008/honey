<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Api;


interface ShippingDataRepositoryInterface
{
    const DATA_TABLE_NAME = 'np_quote_address_data';

    /**
     * @param string $quoteId
     * @param array $data
     * @return mixed
     */
    public function save($quoteId, array $data);

    /**
     * @param string $quoteId
     * @return array
     */
    public function get($quoteId);
}
