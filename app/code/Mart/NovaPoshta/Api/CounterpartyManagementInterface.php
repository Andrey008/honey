<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Api;

/**
 * Interface CounterpartyManagementInterface
 * @package Mart\NovaPoshta\Api
 */
interface CounterpartyManagementInterface extends UpdateInterface
{
    /**
     * @return boolean
     */
    public function updateCounterparties();
}
