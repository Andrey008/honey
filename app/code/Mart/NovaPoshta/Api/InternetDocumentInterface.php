<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Api;


interface InternetDocumentInterface
{
    /**
     * @param \Mart\NovaPoshta\Model\Api\Request $data
     * @return string
     */
    public function getDocumentPrice(\Mart\NovaPoshta\Model\Api\Request $data);

    /**
     * @param \Mart\NovaPoshta\Model\Api\Request $data
     * @return string
     */
    public function getDocumentDeliveryDate(\Mart\NovaPoshta\Model\Api\Request $data);

    /**
     * @param \Magento\Framework\DataObject $data
     * @return mixed
     */
    public function createDocument(\Magento\Framework\DataObject $data);

    /**
     * @param string $tracking
     * @return array
     */
    public function getTrackingInfo($tracking);

    /**
     * @return mixed
     */
    public function getDocumentList();

    /**
     * @param string $ref
     * @return mixed
     */
    public function delete($ref);
}
