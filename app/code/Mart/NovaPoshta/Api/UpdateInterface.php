<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Api;

/**
 * Interface UpdateInterface
 * @package Mart\NovaPoshta\Api
 */
interface UpdateInterface
{
    /**
     * @return array
     */
    public function updateAll();
}
