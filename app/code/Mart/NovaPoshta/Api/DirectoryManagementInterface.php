<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Api;

/**
 * Interface DirectoryManagementInterface
 * @package Mart\NovaPoshta\Api
 */
interface DirectoryManagementInterface extends UpdateInterface
{
    /**
     * @return array
     */
    public function getCargoTypes();

    /**
     * @return array
     */
    public function getServiceTypes();

    /**
     * @return mixed
     */
    public function updateCargoTypes();

    /**
     * @return mixed
     */
    public function updateServiceTypes();
}
