<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Api;

interface DirectoryRepositoryInterface
{
    /**
     * @return \Mart\NovaPoshta\Model\ResourceModel\Directory\CargoType\Collection
     */
    public function getAllCargoTypes();

    /**
     * @return \Mart\NovaPoshta\Model\ResourceModel\Directory\ServiceType\Collection
     */
    public function getAllServiceTypes();

    /**
     * @param string $ref
     * @return \Mart\NovaPoshta\Model\Directory\ServiceType
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getServiceType($ref);
}
