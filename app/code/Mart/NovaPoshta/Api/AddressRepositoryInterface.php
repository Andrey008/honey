<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Api;

interface AddressRepositoryInterface
{
    /**
     * @param bool $withAreas
     * @return \Mart\NovaPoshta\Model\ResourceModel\Address\City\Collection
     */
    public function getAllCities($withAreas = false);

    /**
     * @return \Mart\NovaPoshta\Model\ResourceModel\Address\Area\Collection
     */
    public function getAllAreas();

    /**
     * @param string $ref
     * @return \Mart\NovaPoshta\Model\Address\Area
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getArea($ref);

    /**
     * @param string $ref
     * @return \Mart\NovaPoshta\Model\Address\City
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCity($ref);

    /**
     * @param string $ref
     * @return \Mart\NovaPoshta\Model\Address\Warehouse
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getWarehouse($ref);

    /**
     * @param string $cityRef
     * @param int $number
     * @return \Mart\NovaPoshta\Model\Address\Warehouse
     */
    public function getWarehouseByCityAndNumber($cityRef, $number);
}
