<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Api;

interface CounterpartyRepositoryInterface
{
    /**
     * @param string $ref
     * @return \Mart\NovaPoshta\Model\Counterparty\Counterparty
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCounterpartyByRef($ref);

    /**
     * @param string $ref
     * @return \Mart\NovaPoshta\Model\Counterparty\ContactPerson
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPersonByRef($ref);

    /**
     * @return \Mart\NovaPoshta\Model\ResourceModel\Counterparty\ContactPerson\Collection
     */
    public function getAllPersons();
}
