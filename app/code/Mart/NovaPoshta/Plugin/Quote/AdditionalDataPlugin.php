<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Plugin\Quote;

use Mart\NovaPoshta\Api\AddressRepositoryInterface;
use Mart\NovaPoshta\Api\ShippingDataRepositoryInterface;
use Mart\NovaPoshta\Helper\Data;

class AdditionalDataPlugin
{
    /**
     * @var ShippingDataRepositoryInterface
     */
    protected $shippingDataManagement;

    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * AdditionalDataPlugin constructor.
     *
     * @param ShippingDataRepositoryInterface $shippingDataManagement
     * @param AddressRepositoryInterface $addressRepository
     */
    public function __construct(
        ShippingDataRepositoryInterface $shippingDataManagement,
        AddressRepositoryInterface $addressRepository,
        Data $helper
    ) {
        $this->shippingDataManagement = $shippingDataManagement;
        $this->addressRepository = $addressRepository;
        $this->helper = $helper;
    }

    /**
     * @param $quoteId
     * @param array $data
     */
    protected function saveData($quoteId, array $data)
    {
        $this->shippingDataManagement->save($quoteId, $data);
    }

    /**
     * @param $quoteId
     * @return array|mixed|string
     */
    public function getAdditionalData($quoteId)
    {
        return $this->shippingDataManagement->get($quoteId);
    }

    /**
     * @return array
     */
    protected function getNpCustomAttributes()
    {
        return $this->helper->getCustomAttributesList();
    }
}
