<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Plugin\Quote;

class SaveShippingData extends AdditionalDataPlugin
{
    /**
     * @param \Magento\Checkout\Api\ShippingInformationManagementInterface $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     * @return array
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Api\ShippingInformationManagementInterface $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $data = $addressInformation->getShippingAddress()->getExtensionAttributes();
        $data = $data instanceof \Magento\Framework\Api\AbstractSimpleObject ? $data->__toArray() : [];
        $this->saveData($cartId, $data);


        return [$cartId, $addressInformation];
    }
}
