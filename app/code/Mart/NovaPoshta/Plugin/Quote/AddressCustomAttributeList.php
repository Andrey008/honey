<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Plugin\Quote;

class AddressCustomAttributeList
{
    /**
     * @var \Mart\NovaPoshta\Helper\Data
     */
    protected $helper;

    /**
     * AddressCustomAttributeList constructor.
     *
     * @param \Mart\NovaPoshta\Helper\Data $helper
     */
    public function __construct(\Mart\NovaPoshta\Helper\Data $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\CustomAttributeListInterface $subject
     * @param array $result
     * @return array
     */
    public function afterGetAttributes(
        \Magento\Quote\Model\Quote\Address\CustomAttributeListInterface $subject,
        array $result
    ) {
        return array_merge($result, array_flip($this->helper->getCustomAttributesList()));
    }
}
