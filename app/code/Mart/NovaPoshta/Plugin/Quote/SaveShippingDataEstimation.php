<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Plugin\Quote;

use Mart\NovaPoshta\Api\AddressRepositoryInterface;
use Mart\NovaPoshta\Api\ShippingDataRepositoryInterface;
use Mart\NovaPoshta\Helper\Data;
use Magento\Checkout\Model\Session;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\ShipmentEstimationInterface;
use Magento\Quote\Api\ShippingMethodManagementInterface;

/**
 * Class SaveShippingDataEstimation
 * @package Mart\NovaPoshta\Plugin\Quote
 */
class SaveShippingDataEstimation extends AdditionalDataPlugin
{
    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $customerAddressRepository;

    /**
     * SaveShippingDataEstimation constructor.
     *
     * @param ShippingDataRepositoryInterface $shippingDataManagement
     * @param AddressRepositoryInterface $addressRepository
     * @param \Magento\Customer\Api\AddressRepositoryInterface $customerAddressRepository
     * @param Data $helper
     */
    public function __construct(
        ShippingDataRepositoryInterface $shippingDataManagement,
        AddressRepositoryInterface $addressRepository,
        \Magento\Customer\Api\AddressRepositoryInterface $customerAddressRepository,
        Data $helper
    ) {
        $this->customerAddressRepository = $customerAddressRepository;
        parent::__construct($shippingDataManagement, $addressRepository, $helper);
    }

    /**
     * @param ShipmentEstimationInterface $shipmentEstimation
     * @param $cartId
     * @param AddressInterface $address
     * @return array
     */
    public function beforeEstimateByExtendedAddress(
        ShipmentEstimationInterface $shipmentEstimation,
        $cartId,
        AddressInterface $address
    ) {
        $data = [];
        $attributes = $address->getCustomAttributes();
        foreach ($attributes as $attribute) {
            if (in_array($attribute->getAttributeCode(), $this->getNpCustomAttributes()))
                $data[$attribute->getAttributeCode()] = $attribute->getValue()['value'] ?? '';
        }
        $this->saveData($cartId, $data);

        return [
            $cartId,
            $address
        ];
    }

    /**
     * @param ShippingMethodManagementInterface $shipmentEstimation
     * @param $cartId
     * @param $addressId
     * @return array
     */
    public function beforeEstimateByAddressId(
        ShippingMethodManagementInterface $shipmentEstimation,
        $cartId,
        $addressId
    ) {
        $data = [];
        $address = $this->customerAddressRepository->getById($addressId);
        $attributes = $address->getCustomAttributes();
        foreach ($attributes as $attribute) {
            if (in_array($attribute->getAttributeCode(), $this->getNpCustomAttributes()))
                $data[$attribute->getAttributeCode()] = $attribute->getValue()['value'] ?? $attribute->getValue();
        }
        $this->saveData($cartId, $data);

        return [
            $cartId,
            $addressId
        ];
    }
}
