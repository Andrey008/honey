<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Plugin\Customer;

class AddressAttributesLoad
{
    /**
     * @var \Magento\Customer\Api\Data\AddressExtensionFactory
     */
    protected $extensionFactory;

    /**
     * AddressAttributesLoad constructor.
     *
     * @param \Magento\Customer\Api\Data\AddressExtensionFactory $orderAddressExtensionFactory
     */
    public function __construct(\Magento\Customer\Api\Data\AddressExtensionFactory $orderAddressExtensionFactory)
    {
        $this->extensionFactory = $orderAddressExtensionFactory;
    }

    /**
     * @param \Magento\Customer\Api\Data\AddressInterface $entity
     * @param \Magento\Customer\Api\Data\AddressExtensionInterface|null $extension
     * @return \Magento\Customer\Api\Data\AddressExtensionInterface
     */
    public function afterGetExtensionAttributes(
        \Magento\Customer\Api\Data\AddressInterface $entity,
        \Magento\Customer\Api\Data\AddressExtensionInterface $extension = null
    ) {
        if ($extension === null) {
            $extension = $this->extensionFactory->create();
        }

        return $extension;
    }
}
