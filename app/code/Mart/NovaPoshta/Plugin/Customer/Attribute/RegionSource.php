<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Plugin\Customer\Attribute;

use Mart\NovaPoshta\Api\AddressRepositoryInterface;
use Mart\NovaPoshta\Model\Address\Area;
use Mart\NovaPoshta\Model\ResourceModel\Address\Area\Collection;
use Magento\Customer\Model\ResourceModel\Address\Attribute\Source\Region;

class RegionSource
{
    /**
     * @var Collection|null
     */
    private $areas;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * RegionSource constructor.
     *
     * @param AddressRepositoryInterface $addressRepository
     */
    public function __construct(
        AddressRepositoryInterface $addressRepository
    ) {
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param Region $subject
     * @param array $options
     * @return array
     */
    public function afterGetAllOptions(Region $subject, array $options) : array
    {
        return array_merge($options, $this->getUAAreas());
    }

    /**
     * @param \Magento\Directory\Model\ResourceModel\Region\Collection $subject
     * @param array $options
     * @return array
     */
    public function afterToOptionArray(\Magento\Directory\Model\ResourceModel\Region\Collection $subject, array $options)
    {
        return array_merge($options, $this->getUAAreas());
    }

    /**
     * @param \Magento\Customer\CustomerData\SectionSourceInterface $subject
     * @param array $output
     * @return array
     */
    public function afterGetSectionData(\Magento\Customer\CustomerData\SectionSourceInterface $subject, array $output)
    {
        foreach ($this->getUAAreas() as $area) {
            $output['UA']['regions'][$area['value']] = [
                'code' => $area['area_ref'],
                'name' => $area['label'],
            ];
        }
        return $output;
    }

    /**
     * @return array
     */
    protected function getUAAreas() : array
    {
        if (!isset($this->areas)) {
            $this->areas = [];
            /** @var Area $area */
            foreach ($this->addressRepository->getAllAreas() as $area) {
                $this->areas[] = [
                    'value' => $area->getRef(),
                    'area_ref' => $area->getRef(),
                    'title' => $area->getDescription(),
                    'label' => $area->getDescription(),
                    'country_id' => 'UA',
                ];
            }
            return $this->areas;
        }
        return $this->areas;
    }
}
