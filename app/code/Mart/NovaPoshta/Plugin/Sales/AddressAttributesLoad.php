<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Plugin\Sales;

class AddressAttributesLoad
{
    /**
     * @var \Magento\Sales\Api\Data\OrderAddressExtensionFactory
     */
    protected $extensionFactory;

    /**
     * AddressAttributesLoad constructor.
     *
     * @param \Magento\Sales\Api\Data\OrderAddressExtensionFactory $orderAddressExtensionFactory
     */
    public function __construct(\Magento\Sales\Api\Data\OrderAddressExtensionFactory $orderAddressExtensionFactory)
    {
        $this->extensionFactory = $orderAddressExtensionFactory;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderAddressInterface $entity
     * @param \Magento\Sales\Api\Data\OrderAddressExtensionInterface|null $extension
     * @return \Magento\Sales\Api\Data\OrderAddressExtensionInterface
     */
    public function afterGetExtensionAttributes(
        \Magento\Sales\Api\Data\OrderAddressInterface $entity,
        \Magento\Sales\Api\Data\OrderAddressExtensionInterface $extension = null
    ) {
        if ($extension === null) {
            $extension = $this->extensionFactory->create();
        }

        return $extension;
    }
}
