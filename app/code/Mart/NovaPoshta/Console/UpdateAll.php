<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Console;

use Mart\NovaPoshta\Api\UpdateInterface;
use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateAll extends Command
{
    /**
     * @var UpdateInterface[]
     */
    protected $updateEntities = [];

    /**
     * UpdateAll constructor.
     *
     * @param array $updateEntities
     * @param null $name
     */
    public function __construct(
        array $updateEntities = [],
        $name = null
    ) {
        $this->updateEntities = $updateEntities;
        parent::__construct($name);
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('novaposhta:update')
            ->setDescription('NovaPoshta references update');

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     * @throws LocalizedException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Start NovaPosha update</info>');
        $start = microtime(true);
        foreach ($this->updateEntities as $name => $entity) {
            if (! $entity instanceof UpdateInterface) {
                $class = get_class($entity);
                throw new LocalizedException(
                    __('UpdateEntity must be instance of Mart\NovaPoshta\Api\UpdateInterface, %1 given', $class)
                );
            }
            $output->writeln("<info>Start to update $name </info>");
            $result = $entity->updateAll();
            foreach ($result as $entityName => $error) {
                if ($error)
                    $output->writeln("<error>$entityName error - $error</error>");
            }
        }
        $end = microtime(true);
        $workingTime = round($end - $start, 2);
        $output->writeln("<info>Update finished in $workingTime seconds.</info>");
        return null;
    }
}
