define([
    'Magento_Ui/js/form/element/select',
    'uiRegistry'
], function (Component, registry) {
    'use strict';

    return Component.extend({
        defaults: {
            skipValidation: false,
            imports: {
                update: '${ $.parentName }.region_id:value'
            }
        },

        initInput: function () {
            this._super();
            registry.get(this.customName, function (input) {
                input.validation['required-entry'] = true;
                input.required(true);
            });
            return this;
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            var region = registry.get(this.parentName + '.' + 'region_id'),
                options = region.indexedOptions,
                option;

            if (!value) {
                return;
            }
            option = options[value];

            if (typeof option === 'undefined') {
                return;
            }
        },

        /**
         *
         * @param value
         * @param field
         */
        filter: function (value, field) {
            var region = registry.get(this.parentName + '.' + 'region_id'),
                option;

            if (region) {
                option = region.indexedOptions[value];

                if (!option) {
                    // hide select and corresponding text input field if city must not be shown for selected region
                    this.setVisible(false);
                } else {
                    value = option[this.filterBy.field];
                }
            } else {
                this.setVisible(false);
                this.toggleInput(true);
            }
            this._super(value, field);
        }
    });
});