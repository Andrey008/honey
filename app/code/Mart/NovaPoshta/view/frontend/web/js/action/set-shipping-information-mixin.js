define([
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/checkout-data'
], function (wrapper, quote, checkoutData) {
    'use strict';

    return function (setShippingInformationAction) {

        var warehouseMethodCodes = ['DoorsWarehouse', 'WarehouseWarehouse'];

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var shippingAddress = quote.shippingAddress(),
                shippingMethod = quote.shippingMethod();
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }
            if (shippingAddress['customAttributes'] === undefined) {
                shippingAddress['customAttributes'] = {};
            }

            var cityId = shippingAddress.customAttributes['city_id'];
            var warehouseId = shippingAddress.customAttributes['warehouse_id'];
            if (cityId) {
                shippingAddress['extension_attributes']['city_id'] = cityId['value']['value'];
            }
            if (warehouseId && warehouseMethodCodes.indexOf(shippingMethod.method_code) >= 0) {
                shippingAddress['extension_attributes']['warehouse_id'] = warehouseId['value']['value'];
            } else {
                var address = checkoutData.getShippingAddressFromData();
                delete address.warehouse_id;
                checkoutData.setShippingAddressFromData(address);
                delete shippingAddress.customAttributes['warehouse_id'];
            }
            delete shippingAddress.customAttributes['city_id'];
            // delete shippingAddress.region_id;
            return originalAction();
        });
    };
});