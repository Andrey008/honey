define([
    'uiComponent',
    'Magento_Checkout/js/model/shipping-rates-validator',
    'Magento_Checkout/js/model/shipping-rates-validation-rules',
    '../../model/shipping-rates-validator/novaposhta',
    '../../model/shipping-rates-validation-rules/novaposhta'
], function (
    Component,
    defaultShippingRatesValidator,
    defaultShippingRatesValidationRules,
    novaposhtaShippingRatesValidator,
    novaposhtaShippingRatesValidationRules
) {
    'use strict';

    var carrierCode = 'novaposhta';

    defaultShippingRatesValidator.registerValidator(carrierCode, novaposhtaShippingRatesValidator);
    defaultShippingRatesValidationRules.registerRules(carrierCode, novaposhtaShippingRatesValidationRules);

    return Component;
});
