define([
    'mage/utils/wrapper',
    'uiRegistry'
], function (wrapper, registry) {
    'use strict';

    return function (addressAction) {
        var scope = 'checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset';

        return wrapper.wrap(addressAction, function (action, addressData) {
            if (addressData.country_id == 'UA') {
                delete addressData.region.region_id;
                delete addressData.region.region_code;

                var result = action(addressData);
                var warehouseField = registry.get(scope + '.warehouse_id');
                var warehouses = warehouseField.indexedOptions;
                result.customAttributes = result.customAttributes || {};
                var warehouse = warehouses[addressData.warehouse_id];
                if (addressData.warehouse_id && !warehouse) {
                    warehouse = warehouseField.initialOptions.find(function (e) {
                        return e.value === addressData.warehouse_id;
                    })
                }
                if (warehouse) {
                    result.customAttributes.warehouse_id = {
                        attribute_code: 'warehouse_id',
                        value: {
                            value: addressData.warehouse_id,
                            toString: function () {return warehouse.label}
                        }
                    };
                }
                var cityField = registry.get(scope + '.city_id');
                var cities = cityField.indexedOptions;
                var city = cities[addressData.city_id];
                if (addressData.city_id && !city) {
                    city = cityField.initialOptions.find(function (e) {
                        return e.value === addressData.city_id;
                    })
                }
                if (city) {
                    result.customAttributes.city_id = {
                        attribute_code: 'city_id',
                        value: {
                            value: addressData.city_id,
                            toString: function () {return ''}
                        }
                    };
                    result.city = city.label;
                }
                return result;
            }
            return action(addressData);
        });
    }
});