define([
    'jquery',
    'mage/utils/wrapper',
    'uiRegistry'
], function ($, wrapper, registry) {
    'use strict';

    var selectAttributes = ['warehouse_id', 'city_id'];
    var scope = 'checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset';

    return function (address) {

        return wrapper.wrap(address, function (originalAction, addressData) {
            $.each(addressData.custom_attributes, function (i, e) {
                if (selectAttributes.indexOf(i) >= 0) {
                    if (i == 'city_id') {
                        e['value'] = {
                            value: e.value,
                            toString: function () {return ''}
                        };
                        return;
                    }
                    var field = registry.get(scope + '.' + i);
                    var option = field.indexedOptions[e.value];
                    if (!option) {
                        option = field.initialOptions.find(function (option) {
                            return option.value === e.value;
                        })
                    }
                    e['value'] = {
                        value: e.value,
                        toString: function () {return option.label;}
                    };
                }
            });
            return originalAction(addressData);
        })

    }
});