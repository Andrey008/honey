define([
    'Magento_Ui/js/form/element/select',
    'ko',
    'underscore',
    'Magento_Checkout/js/model/quote',
    'uiRegistry'
], function (Component, ko, _, quote, registry) {
    'use strict';

    var warehouseMethodCodes = ['DoorsWarehouse', 'WarehouseWarehouse'],
        isWh = true;

    return Component.extend({
        defaults: {
            skipValidation: false,
            isWhShipping: ko.observable(),
            quoteWeight: 0,
            itemMaxWeight: 0,
            imports: {
                update: '${ $.parentName }.city_id:value'
            }
        },

        initialize: function () {
            this._super();
            quote.shippingMethod.subscribe(this.updateVisibility.bind(this));
            this.calculateTotalWeight();
            return this;
        },

        calculateTotalWeight: function () {
            var self = this;
            _.each(quote.getItems(), function (e) {
                self.quoteWeight += +e.row_weight;
                if (self.itemMaxWeight < +e.weight) {
                    self.itemMaxWeight = +e.weight;
                }
            });
        },

        initInput: function () {
            this._super();
            registry.get(this.customName, function (input) {
                input.validation['required-entry'] = true;
                input.required(true);
                input.visible = ko.observable(false);
            });
            return this;
        },

        updateVisibility: function (value) {
            isWh = value && warehouseMethodCodes.indexOf(value.method_code) >= 0;
            this.isWhShipping(isWh);
            this.required(!isWh);
            this.setVisible(isWh && !_.isEmpty(this.indexedOptions));
            this.disabled(!isWh);
            this.toggleInput(_.isEmpty(this.indexedOptions));
            this.validation = _.omit(this.validation, 'required-entry');
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            var city = registry.get(this.parentName + '.' + 'city_id'),
                options = city.indexedOptions,
                option;

            if (!value) {
                return;
            }
            option = options[value];

            if (typeof option === 'undefined') {
                return;
            }

            if (this.skipValidation) {
                this.validation['required-entry'] = false;
                this.required(false);
            } else {
                this.required(true);
                this.validation['required-entry'] = true;
            }
        },

        /**
         *
         * @param value
         * @param field
         */
        filter: function (value, field) {
            var city = registry.get(this.parentName + '.' + 'city_id'),
                option;

            if (city) {
                option = city.indexedOptions[value];

                this.setVisible(this.isWhShipping() && (!!option || value));

                if (this.customEntry) {// eslint-disable-line max-depth
                    this.toggleInput(!option);
                }
            }
            this._super(value, field);
        },

        setOptions: function (data) {
            this._super(data);
            this.filterByMaxWeight();
            return this;
        },

        filterByMaxWeight: function () {
            var self = this;
            var options = _.filter(this.indexedOptions, function (e) {
                var maxWeight = +e.max_weight || +e.place_max_weight;
                return !maxWeight //no restriction
                    || self.quoteWeight <= maxWeight //all products as 1 parcel
                    || self.itemMaxWeight <= maxWeight; // products will be separated
            });
            this.indexedOptions = options;
            this.options(options);
        },

        /**
         * Change visibility for input.
         *
         * @param {Boolean} isVisible
         */
        toggleInput: function (isVisible) {
            var self = this;
            registry.get(this.customName, function (input) {
                input.setVisible(self.isWhShipping() && isVisible);
            });
        }
    });
});