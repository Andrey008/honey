define([
    
], function () {
    'use strict';

    var mixin = {

    };

    return function (target) {
        return target.extend(mixin);
    }
});