define([], function () {
    'use strict';

    return {
        /**
         * @return {Object}
         */
        getRules: function () {
            return {
                'city_id': {
                    'required': true
                }
            }
        }
    };
});