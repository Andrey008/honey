var config = {
    'config': {
        'mixins': {
            'Magento_Checkout/js/model/new-customer-address': {
                'Mart_NovaPoshta/js/model/new-customer-address-mixin': true
            },
            'Magento_Customer/js/model/customer/address': {
                'Mart_NovaPoshta/js/model/customer/address-mixin': true
            },
            'Magento_Checkout/js/action/set-shipping-information': {
                'Mart_NovaPoshta/js/action/set-shipping-information-mixin': true
            }
        }
    }
};
