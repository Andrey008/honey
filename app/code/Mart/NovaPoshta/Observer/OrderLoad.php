<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Observer;

use Mart\NovaPoshta\Plugin\Quote\AdditionalDataPlugin;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class OrderLoad extends AdditionalDataPlugin implements ObserverInterface
{
    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getOrder();
        if ($order->getId()) {
            $data = $this->getAdditionalData($order->getQuoteId());
            if (!empty($data)) {
                $address = $order->getShippingAddress();
                $extensionAttributes = $address->getExtensionAttributes();
                foreach ($data as $key => $value) {
                    $extensionAttributes->setData($key, $value);
                    $address->setData($key, $value);
                }
                $address->setExtensionAttributes($extensionAttributes);
            }
        }
        return $this;
    }
}
