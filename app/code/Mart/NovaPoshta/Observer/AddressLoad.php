<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Observer;

use Mart\NovaPoshta\Plugin\Quote\AdditionalDataPlugin;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class AddressLoad extends AdditionalDataPlugin implements ObserverInterface
{
    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Address $address */
        $address = $observer->getAddress();
        if ($address->getId()) {
            $data = $this->getAdditionalData($address->getOrder()->getQuoteId());
            if (!empty($data)) {
                $extensionAttributes = $address->getExtensionAttributes();
                foreach ($data as $key => $value) {
                    $extensionAttributes->setData($key, $value);
                    $address->setData($key, $value);
                }
                $address->setExtensionAttributes($extensionAttributes);
            }
        }
        return $this;
    }
}
