<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Setup;

use Mart\NovaPoshta\Model\Carrier\NovaPoshta;
use Magento\Directory\Helper\Data;
use Magento\Directory\Model\Currency;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\ScopeInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @var string
     */
    private $currencySetupUrl = 'http://free.currencyconverterapi.com/api/v5/convert?q={{FROM_TO}}&compact=y';

    /**
     * Category setup factory
     *
     * @var \Magento\Customer\Setup\CustomerSetup
     */
    private $customerSetup;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Config\Model\ResourceModel\Config
     */
    private $resourceConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * InstallData constructor.
     *
     * @param \Magento\Customer\Setup\CustomerSetup $customerSetup
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param Currency $currency
     */
    public function __construct(
        \Magento\Customer\Setup\CustomerSetup $customerSetup,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\Currency $currency
    ) {
        $this->customerSetup = $customerSetup;
        $this->curl = $curl;
        $this->scopeConfig = $scopeConfig;
        $this->resourceConfig = $resourceConfig;
        $this->storeManager = $storeManager;
        $this->currency = $currency;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        /**
         * Add attributes to the eav/attribute
         */
        $attributes = [
            'city_id' => [
                'type' => 'varchar',
                'label' => 'City',
                'input' => 'select',
                'required' => false,
                'system' => false,
                'sort_order' => 105,
                'position' => 105,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false,
                'source' => 'Mart\NovaPoshta\Model\ResourceModel\Address\Attribute\Source\City',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            ],
            'warehouse_id' => [
                'type' => 'varchar',
                'label' => 'Warehouse',
                'input' => 'select',
                'system' => false,
                'sort_order' => 106,
                'position' => 106,
                'required' => false,
                'is_used_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false,
                'source' => 'Mart\NovaPoshta\Model\ResourceModel\Address\Attribute\Source\Warehouse',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            ]
        ];
        foreach ($attributes as $code => $data) {
            $this->customerSetup->addAttribute(
                \Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY,
                $code,
                $data
            );
        }

        foreach ($attributes as $code => $data) {
            $attribute = $this->customerSetup->getEavConfig()->getAttribute(
                \Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY,
                $code);
            $attribute->setData(
                'used_in_forms',
                [
                    'adminhtml_checkout',
                    'adminhtml_customer_address',
                    'customer_account_create',
                    'customer_address_edit',
                    'customer_register_address'
                ]
            );
            $attribute->save();
        }

        $defaultCurrencies = $this->scopeConfig->getValue(
            Currency::XML_PATH_CURRENCY_ALLOW
        );
        $currencies = array_flip(explode(',', $defaultCurrencies));
        foreach ($this->storeManager->getStores(true) as $store) {
            $storeCurrencies = $this->scopeConfig->getValue(
                Currency::XML_PATH_CURRENCY_ALLOW,
                ScopeInterface::SCOPE_STORE,
                $store->getId()
            );
            if ($storeCurrencies) {
                $storeCurrencies = explode(',', $storeCurrencies);
                $currencies = array_merge($currencies, array_flip($storeCurrencies));
            }
        }

        unset($currencies['UAH']);
        foreach ($currencies as $currency => $index) {
            if (!$this->currency->getCurrencyRates(NovaPoshta::NP_CURRENCY, $currency)) {
                $param = NovaPoshta::NP_CURRENCY . '_' . $currency;
                $url = str_replace('{{FROM_TO}}', $param, $this->currencySetupUrl);
                $this->curl->setTimeout(100);
                $this->curl->get($url);
                try {
                    $rates = \Zend_Json::decode($this->curl->getBody());
                    if (isset($rates[$param]['val'])) {
                        $rate = $rates[$param]['val'];
                        $this->currency->saveRates([
                            NovaPoshta::NP_CURRENCY => [
                                $currency => $rate
                            ]
                        ]);
                        $this->currency->saveRates([
                            $currency => [
                                NovaPoshta::NP_CURRENCY => (float)1/$rate
                            ]
                        ]);
                    }
                } catch (\Zend_Json_Exception $e) {
                    // Invalid response
                }

            }
        }
        $countries = $this->scopeConfig->getValue(Data::OPTIONAL_ZIP_COUNTRIES_CONFIG_PATH);
        $countries = explode(',', $countries);
        if (!in_array('UA', $countries)) {
            $countries[] = 'UA';
            $this->resourceConfig->saveConfig(Data::OPTIONAL_ZIP_COUNTRIES_CONFIG_PATH, implode(',' , $countries));
        }

        $setup->endSetup();
    }
}
