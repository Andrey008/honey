<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Setup;

use Mart\NovaPoshta\Model\Address\Area;
use Mart\NovaPoshta\Model\Address\City;
use Mart\NovaPoshta\Model\Address\Warehouse;
use Mart\NovaPoshta\Model\Address\WarehouseType;
use Mart\NovaPoshta\Model\Counterparty\ContactPerson;
use Mart\NovaPoshta\Model\Counterparty\Counterparty;
use Mart\NovaPoshta\Model\Directory\CargoType;
use Mart\NovaPoshta\Model\Directory\ServiceType;
use Mart\NovaPoshta\Api\ShippingDataRepositoryInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $areaTable = $setup->getConnection()
            ->newTable($setup->getTable(Area::TABLE_NAME))
            ->addColumn(
                'entity_id',
                Table::TYPE_SMALLINT,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Area ID'
            )
            ->addColumn(
                'ref',
                Table::TYPE_TEXT,
                36,
                [
                    'nullable' => false,
                ],
                'Area ref'
            )
            ->addColumn(
                'center_ref',
                Table::TYPE_TEXT,
                36,
                [
                    'nullable' => false,
                ],
                'Area center city ref'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                50,
                [],
                'Area name'
            )
            ->addIndex(
                $setup->getIdxName(WarehouseType::TABLE_NAME, ['ref'], AdapterInterface::INDEX_TYPE_UNIQUE),
                ['ref'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            );
        $setup->getConnection()->createTable($areaTable);

        $cityTable = $setup->getConnection()
            ->newTable($setup->getTable(City::TABLE_NAME))
            ->addColumn(
                'entity_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'City ID'
            )
            ->addColumn(
                'ref',
                Table::TYPE_TEXT,
                36,
                [
                    'nullable' => false,
                ],
                'City Ref'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                50,
                [],
                'City Name'
            )
            ->addColumn(
                'description_ru',
                Table::TYPE_TEXT,
                50,
                [],
                'City Name on Russian'
            )
            ->addColumn(
                'type',
                Table::TYPE_TEXT,
                36,
                [],
                'City Type'
            )
            ->addColumn(
                'type_ru',
                Table::TYPE_TEXT,
                36,
                [],
                'City Type on Russian'
            )
            ->addColumn(
                'area_ref',
                Table::TYPE_TEXT,
                36,
                [
                    'nullable' => false,
                ],
                'Area Ref'
            )
            ->addColumn(
                'city_code',
                Table::TYPE_SMALLINT,
                null,
                [],
                'City Code'
            )
            ->addColumn(
                'is_branch',
                Table::TYPE_BOOLEAN,
                null,
                [],
                'Is City Branch'
            );
        for ($i = 1; $i <= 7; $i++) {
            $cityTable->addColumn(
                "delivery_$i",
                Table::TYPE_BOOLEAN,
                null,
                ['nullable' => false],
                'Has delivery on day of week'
            );
        }
        $cityTable->addForeignKey(
            $setup->getFkName(City::TABLE_NAME, 'area_ref', Area::TABLE_NAME, 'ref'),
            'area_ref',
            $setup->getTable(Area::TABLE_NAME),
            'ref',
            Table::ACTION_CASCADE
        );
        $cityTable->addIndex(
            $setup->getIdxName(City::TABLE_NAME, ['ref'], AdapterInterface::INDEX_TYPE_UNIQUE),
            'ref',
            ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
        );
        $setup->getConnection()->createTable($cityTable);

        $warehouseTypeTable = $setup->getConnection()
            ->newTable($setup->getTable(WarehouseType::TABLE_NAME))
            ->addColumn(
                'ref',
                Table::TYPE_TEXT,
                36,
                [
                    'primary' => true,
                    'nullable' => false,
                ],
                'Warehouse Type Ref'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                99,
                ['nullable' => false],
                'Warehouse type description'
            )
            ->addIndex(
                $setup->getIdxName(WarehouseType::TABLE_NAME, ['ref'], AdapterInterface::INDEX_TYPE_UNIQUE),
                ['ref'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            );
        $setup->getConnection()->createTable($warehouseTypeTable);

        $warehouseTable = $setup->getConnection()
            ->newTable($setup->getTable(Warehouse::TABLE_NAME))
            ->addColumn(
                'entity_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true,
                ],
                'Warehouse ID'
            )
            ->addColumn(
                'ref',
                Table::TYPE_TEXT,
                36,
                [
                    'nullable' => false,
                ],
                'Warehouse Ref'
            )
            ->addColumn(
                'city_ref',
                Table::TYPE_TEXT,
                36,
                [
                    'nullable' => false
                ],
                'City Ref'
            )
            ->addColumn(
                'warehouse_key',
                Table::TYPE_INTEGER,
                null,
                [],
                'Warehouse Code'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                99,
                [],
                'Warehouse Name'
            )
            ->addColumn(
                'description_ru',
                Table::TYPE_TEXT,
                99,
                [],
                'Warehouse Name on Russian'
            )
            ->addColumn(
                'type',
                Table::TYPE_TEXT,
                36,
                [],
                'Warehouse Type'
            )
            ->addColumn(
                'number',
                Table::TYPE_INTEGER,
                null,
                [],
                'Warehouse Number'
            )
            ->addColumn(
                'short_address',
                Table::TYPE_TEXT,
                99,
                [],
                'Warehouse Short Address'
            )
            ->addColumn(
                'short_address_ru',
                Table::TYPE_TEXT,
                99,
                [],
                'Warehouse Short Address on Russian'
            )
            ->addColumn(
                'status',
                Table::TYPE_TEXT,
                36,
                [],
                'Warehouse Description on Russian'
            )
            ->addColumn(
                'bicycle_parking',
                Table::TYPE_BOOLEAN,
                null,
                [],
                'Has bicycle parking'
            )
            ->addColumn(
                'longitude',
                Table::TYPE_TEXT,
                50,
                [],
                'Warehouse Longitude'
            )
            ->addColumn(
                'latitude',
                Table::TYPE_TEXT,
                50,
                [],
                'Warehouse Latitude'
            )
            ->addColumn(
                'post_finance',
                Table::TYPE_BOOLEAN,
                null,
                [],
                'Has Post Finance'
            )
            ->addColumn(
                'pos_terminal',
                Table::TYPE_BOOLEAN,
                null,
                [],
                'Has POS-Terminal'
            )
            ->addColumn(
                'international',
                Table::TYPE_BOOLEAN,
                null,
                [],
                'Can make international shipping'
            )
            ->addColumn(
                'max_weight',
                Table::TYPE_INTEGER,
                null,
                [],
                'Cargo max weight'
            )
            ->addColumn(
                'place_max_weight',
                Table::TYPE_INTEGER,
                null,
                [],
                'Cargo max weight'
            )
            ->addColumn(
                'reception_schedule',
                Table::TYPE_TEXT,
                255,
                [],
                'Deliveries receive schedule (json)'
            )
            ->addColumn(
                'delivery_schedule',
                Table::TYPE_TEXT,
                255,
                [],
                'Deliveries send schedule (json)'
            )
            ->addColumn(
                'work_schedule',
                Table::TYPE_TEXT,
                255,
                [],
                'Warehouse schedule (json)'
            )
            ->addColumn(
                'site_key',
                Table::TYPE_SMALLINT,
                null,
                [],
                'Warehouse schedule (json)'
            )
            ->addIndex(
                $setup->getIdxName(Warehouse::TABLE_NAME, ['ref'], AdapterInterface::INDEX_TYPE_UNIQUE),
                ['ref'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->addForeignKey(
                $setup->getFkName(Warehouse::TABLE_NAME, 'type', WarehouseType::TABLE_NAME, 'ref'),
                'type',
                $setup->getTable(WarehouseType::TABLE_NAME),
                'ref',
                Table::ACTION_CASCADE
            );
        $setup->getConnection()->createTable($warehouseTable);

        $cargoTypesTable = $setup->getConnection()
            ->newTable($setup->getTable(CargoType::TABLE_NAME))
            ->addColumn(
                'ref',
                Table::TYPE_TEXT,
                36,
                [
                    'primary' => true,
                    'nullable' => false,
                ],
                'Cargo Type Ref'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                36,
                ['nullable' => false,],
                'Cargo type description'
            )
            ->addIndex(
                $setup->getIdxName(CargoType::TABLE_NAME, ['ref'], AdapterInterface::INDEX_TYPE_UNIQUE),
                ['ref'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            );
        $setup->getConnection()->createTable($cargoTypesTable);

        $serviceTypesTable = $setup->getConnection()
            ->newTable($setup->getTable(ServiceType::TABLE_NAME))
            ->addColumn(
                'ref',
                Table::TYPE_TEXT,
                36,
                [
                    'primary' => true,
                    'nullable' => false,
                ],
                'Service Type Ref'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                36,
                ['nullable' => false,],
                'Service description'
            )
            ->addIndex(
                $setup->getIdxName(ServiceType::TABLE_NAME, ['ref'], AdapterInterface::INDEX_TYPE_UNIQUE),
                ['ref'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            );
        $setup->getConnection()->createTable($serviceTypesTable);

        $counterpartyTable = $setup->getConnection()
            ->newTable($setup->getTable(Counterparty::TABLE_NAME))
            ->addColumn(
                'entity_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Counterparty ID'
            )
            ->addColumn(
                'ref',
                Table::TYPE_TEXT,
                36,
                [
                    'nullable' => false,
                ],
                'Counterparty Ref'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                50,
                ['nullable' => false,],
                'Counterparty description'
            )
            ->addColumn(
                'city',
                Table::TYPE_TEXT,
                36,
                ['nullable' => false,],
                'City'
            )
            ->addColumn(
                'firstname',
                Table::TYPE_TEXT,
                36,
                [],
                'First Name'
            )
            ->addColumn(
                'lastname',
                Table::TYPE_TEXT,
                36,
                [],
                'Last Name'
            )
            ->addColumn(
                'middlename',
                Table::TYPE_TEXT,
                36,
                [],
                'Middle Name'
            )
            ->addColumn(
                'ownership_form_ref',
                Table::TYPE_TEXT,
                36,
                [],
                'Ownership Form Ref'
            )
            ->addColumn(
                'ownership_form_description',
                Table::TYPE_TEXT,
                36,
                [],
                'Ownership Form Description'
            )
            ->addColumn(
                'edrpou',
                Table::TYPE_INTEGER,
                36,
                [],
                'EDRPOU'
            )
            ->addColumn(
                'counterparty_type',
                Table::TYPE_INTEGER,
                36,
                [],
                'Counterparty Type'
            )
            ->addIndex(
                $setup->getIdxName(ServiceType::TABLE_NAME, ['ref'], AdapterInterface::INDEX_TYPE_UNIQUE),
                ['ref'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            );
        $setup->getConnection()->createTable($counterpartyTable);

        $contactPersonTable = $setup->getConnection()
            ->newTable($setup->getTable(ContactPerson::TABLE_NAME))
            ->addColumn(
                'entity_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Contact person ID'
            )
            ->addColumn(
                'ref',
                Table::TYPE_TEXT,
                36,
                [
                    'nullable' => false,
                ],
                'Counterparty Ref'
            )
            ->addColumn(
                'counterparty_ref',
                Table::TYPE_TEXT,
                36,
                [
                    'nullable' => false,
                ],
                'Counterparty Ref'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                50,
                ['nullable' => false,],
                'Counterparty description'
            )
            ->addColumn(
                'phones',
                Table::TYPE_TEXT,
                50,
                [],
                'Phones'
            )
            ->addColumn(
                'email',
                Table::TYPE_TEXT,
                36,
                [],
                'Email'
            )
            ->addColumn(
                'firstname',
                Table::TYPE_TEXT,
                36,
                [],
                'First Name'
            )
            ->addColumn(
                'lastname',
                Table::TYPE_TEXT,
                36,
                [],
                'Last Name'
            )
            ->addColumn(
                'middlename',
                Table::TYPE_TEXT,
                36,
                [],
                'Middle Name'
            )
            ->addIndex(
                $setup->getIdxName(
                    ServiceType::TABLE_NAME,
                    ['ref', 'counterparty_ref'],
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['ref', 'counterparty_ref'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            );
        $setup->getConnection()->createTable($contactPersonTable);

        $additionalTable = $setup->getConnection()
            ->newTable($setup->getTable(ShippingDataRepositoryInterface::DATA_TABLE_NAME))
            ->addColumn(
                'quote_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false,
                    'unsigned' => true,
                    'primary' => true
                ],
                'Quote Address Id'
            )
            ->addColumn(
                'shipping_data',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'NovaPoshta Attributes JSON data'
            )
            ->addIndex(
                $setup->getIdxName(ShippingDataRepositoryInterface::DATA_TABLE_NAME, ['quote_id']),
                ['quote_id']
            )
            ->addForeignKey(
                $setup->getFkName('quote', 'entity_id', ShippingDataRepositoryInterface::DATA_TABLE_NAME, 'quote_id'),
                'quote_id',
                $setup->getTable('quote'),
                'entity_id',
                Table::ACTION_CASCADE
            )
            ->setComment('NovaPoshta additional shipping data');
        $setup->getConnection()->createTable($additionalTable);

        $setup->endSetup();
    }
}
