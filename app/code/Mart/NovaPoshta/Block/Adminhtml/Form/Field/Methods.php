<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Block\Adminhtml\Form\Field;

use Mart\NovaPoshta\Api\DirectoryRepositoryInterface;
use Mart\NovaPoshta\Model\Directory\ServiceType;
use Magento\Framework\View\Element\Context;

class Methods extends \Magento\Framework\View\Element\Html\Select
{
    /**
     * @var DirectoryRepositoryInterface
     */
    protected $_directoryRepository;

    /**
     * Methods constructor.
     *
     * @param Context $context
     * @param DirectoryRepositoryInterface $directoryRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        DirectoryRepositoryInterface $directoryRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_directoryRepository = $directoryRepository;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {
            foreach ($this->_getAllMethods() as $ref => $method) {
                $this->addOption($ref, addslashes($method['name']), $method['params']);
            }
        }
        return parent::_toHtml();
    }

    /**
     * @return array
     */
    private function _getAllMethods()
    {
        $methods = [];
        /** @var ServiceType $serviceType */
        foreach ($this->_directoryRepository->getAllServiceTypes() as $serviceType) {
            $methods[$serviceType->getRef()] = [
                'name' => $serviceType->getDescription(),
                'params' => [],
            ];
        }
        return $methods;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
