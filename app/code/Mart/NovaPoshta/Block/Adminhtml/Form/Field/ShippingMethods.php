<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Block\Adminhtml\Form\Field;

class ShippingMethods extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    private $_methods;

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'ref',
            ['label' => __('Shipping Method'), 'renderer' => $this->_getMethods()]
        );
        $this->addColumn(
            'title',
            ['label' => __('Title')]
        );
        $this->addColumn(
            'order',
            ['label' => __('Sort Order')]
        );
        $this->addColumn(
            'free_shipping_subtotal',
            ['label' => __('Free Shipping Amount Threshold')]
        );
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Method');
    }

    /**
     * @return \Mart\NovaPoshta\Block\Adminhtml\Form\Field\Methods
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getMethods()
    {
        if (!isset($this->_methods)) {
            $this->_methods = $this->getLayout()->createBlock(
                \Mart\NovaPoshta\Block\Adminhtml\Form\Field\Methods::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->_methods;
    }

    /**
     * Prepare existing row data object
     *
     * @param \Magento\Framework\DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $optionExtraAttr = [];
        $optionExtraAttr['option_' . $this->_getMethods()->calcOptionHash($row->getData('ref'))] =
            'selected="selected"';
        $row->setData(
            'option_extra_attrs',
            $optionExtraAttr
        );
    }
}
