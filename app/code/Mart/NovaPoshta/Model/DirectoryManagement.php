<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model;

use Mart\NovaPoshta\Api\DirectoryManagementInterface;
use Mart\NovaPoshta\Model\Directory\CargoType;
use Mart\NovaPoshta\Model\Directory\ServiceType;

class DirectoryManagement extends AbstractManagement implements DirectoryManagementInterface
{
    /**
     * @inheritdoc
     */
    public function getCargoTypes() : array
    {
        return $this->_api->getCargoTypes();
    }

    /**
     * @inheritdoc
     */
    public function getServiceTypes() : array
    {
        return $this->_api->getServiceTypes();
    }

    /**
     * @inheritdoc
     */
    public function updateAll()
    {
        return [
            'Cargo Types' => $this->updateCargoTypes(),
            'Service Types' => $this->updateServiceTypes(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function updateCargoTypes()
    {
        $result = [];
        $data = $this->getCargoTypes();
        try {
            foreach ($data['data'] as $item) {
                $this->resourceConnection->getConnection()->insertOnDuplicate(
                    $this->resourceConnection->getTableName(CargoType::TABLE_NAME),
                    [
                        'ref' => $item['Ref'],
                        'description' => $item['Description'],
                    ],
                    ['ref', 'description']
                );
            }
        } catch (\Exception $e) {
            $result[] = $e->getMessage();
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function updateServiceTypes()
    {
        $result = [];
        $data = $this->getServiceTypes();
        try {
            foreach ($data['data'] as $item) {
                $this->resourceConnection->getConnection()->insertOnDuplicate(
                    $this->resourceConnection->getTableName(ServiceType::TABLE_NAME),
                    [
                        'ref' => $item['Ref'],
                        'description' => $item['Description'],
                    ],
                    ['ref', 'description']
                );
            }
        } catch (\Exception $e) {
            $result[] = $e->getMessage();
        }
        return $result;
    }
}
