<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\ObjectManagerInterface;

abstract class AbstractRepository
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var array
     */
    protected $_collections = [];

    /**
     * @var array
     */
    protected $_entities = [];

    /**
     * DirectoryRepository constructor.
     *
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->_objectManager = $objectManager;
    }

    /**
     * @param string $modelClass
     * @param string $ref
     * @return AbstractModel
     * @throws NoSuchEntityException
     */
    protected function _loadEntity($modelClass, $ref)
    {
        $ref = trim($ref);
        if (!isset($this->_entities[$modelClass][$ref])) {
            /** @var AbstractModel $model */
            $model = $this->_objectManager->create($modelClass);
            /** @var \Mart\NovaPoshta\Model\ResourceModel\AbstractResource $resource */
            $resource = $this->_objectManager->get($model->getResourceName());
            $entityId = $resource->getIdByRef($ref);
            if (!$entityId) {
                throw new NoSuchEntityException(__('%1 with ref "%2" does not exist', $modelClass, $ref));
            }
            $model->load($entityId);
            $this->_entities[$modelClass][$ref] = $model;
        }
        return $this->_entities[$modelClass][$ref];
    }
}
