<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model;

use Mart\NovaPoshta\Api\CounterpartyRepositoryInterface;
use Mart\NovaPoshta\Model\Counterparty\ContactPerson;
use Mart\NovaPoshta\Model\Counterparty\Counterparty;
use Mart\NovaPoshta\Model\ResourceModel\Counterparty\ContactPerson\Collection as ContactPersonCollection;

class CounterpartyRepository extends AbstractRepository implements CounterpartyRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function getCounterpartyByRef($ref)
    {
        return $this->_loadEntity(Counterparty::class, $ref);
    }

    /**
     * @inheritdoc
     */
    public function getPersonByRef($ref)
    {
        return $this->_loadEntity(ContactPerson::class, $ref);
    }

    /**
     * @inheritdoc
     */
    public function getAllPersons()
    {
        if (!isset($this->_collections[ContactPersonCollection::class])) {
            $this->_collections[ContactPersonCollection::class] = $this->_objectManager
                ->create(ContactPersonCollection::class);
        }
        return $this->_collections[ContactPersonCollection::class];
    }
}
