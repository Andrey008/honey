<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Counterparty;

use Magento\Framework\Model\AbstractModel;

/**
 * Class ContactPerson
 * @package Mart\NovaPoshta\Model\Counterparty
 *
 * @method string getRef
 * @method string getPhones
 * @method string getCounterpartyRef
 */
class ContactPerson extends AbstractModel
{
    const TABLE_NAME = 'np_counterparty_contact_person';

    /**
     * Model constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init(\Mart\NovaPoshta\Model\ResourceModel\Counterparty\ContactPerson::class);
    }

    public function getFullName()
    {
        return $this->getLastname() . ' ' . $this->getFirstname() . ' ' . $this->getMiddlename();
    }
}
