<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Counterparty;

use Magento\Framework\Model\AbstractModel;

class Counterparty extends AbstractModel
{
    const TABLE_NAME = 'np_counterparty';

    /**
     * Model constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init(\Mart\NovaPoshta\Model\ResourceModel\Counterparty\Counterparty::class);
    }
}
