<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Directory;

use Magento\Framework\Model\AbstractModel;

/**
 * Class ServiceType
 * @package Mart\NovaPoshta\Model\Directory
 * @method string getRef
 * @method string|null getDescription
 */
class ServiceType extends AbstractModel
{
    const TABLE_NAME = 'np_service_type';

    /**
     * Model constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init(\Mart\NovaPoshta\Model\ResourceModel\Directory\ServiceType::class);
    }
}
