<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Directory;

use Magento\Framework\Model\AbstractModel;

/**
 * Class CargoType
 * @package Mart\NovaPoshta\Model\Directory
 * @method string getRef
 * @method string|null getDescription
 */
class CargoType extends AbstractModel
{
    const TABLE_NAME = 'np_cargo_type';

    /**
     * Model constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init(\Mart\NovaPoshta\Model\ResourceModel\Directory\CargoType::class);
    }
}
