<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Config\Source;

class Methods extends AbstractDirectory
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        /** @var \Mart\NovaPoshta\Model\Directory\ServiceType $serviceType */
        foreach ($this->_directoryRepository->getAllServiceTypes() as $serviceType) {
            $options[] = [
                'value' => $serviceType->getRef(),
                'label' => $serviceType->getDescription(),
            ];
        }
        return $options;
    }
}
