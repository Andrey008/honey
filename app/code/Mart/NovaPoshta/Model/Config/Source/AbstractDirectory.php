<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Config\Source;

use Mart\NovaPoshta\Api\DirectoryRepositoryInterface;
use Magento\Framework\Option\ArrayInterface;

abstract class AbstractDirectory implements ArrayInterface
{
    /**
     * @var DirectoryRepositoryInterface
     */
    protected $_directoryRepository;

    /**
     * Methods constructor.
     *
     * @param DirectoryRepositoryInterface $directoryRepository
     */
    public function __construct(
        DirectoryRepositoryInterface $directoryRepository
    ) {
        $this->_directoryRepository = $directoryRepository;
    }
}
