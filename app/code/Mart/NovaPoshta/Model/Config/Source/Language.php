<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Language implements ArrayInterface
{
    const LANG_UA = 'ua';
    const LANG_RU = 'ru';


    public function toOptionArray()
    {
        return [
            [
                'value' => self::LANG_UA,
                'label' => __('Ukrainian'),
            ],
            [
                'value' => self::LANG_RU,
                'label' => __('Russian'),
            ],
        ];
    }
}
