<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Config\Source;

use Mart\NovaPoshta\Api\CounterpartyRepositoryInterface;
use Magento\Framework\Option\ArrayInterface;

class ContactPerson implements ArrayInterface
{
    /**
     * @var CounterpartyRepositoryInterface
     */
    protected $_counterpartyRepository;

    /**
     * ContactPerson constructor.
     *
     * @param CounterpartyRepositoryInterface $counterpartyRepository
     */
    public function __construct(
        CounterpartyRepositoryInterface $counterpartyRepository
    ) {
        $this->_counterpartyRepository = $counterpartyRepository;
    }

    public function toOptionArray()
    {
        $options = [];
        foreach ($this->_counterpartyRepository->getAllPersons() as $person) {
            $title = $person->getLastname() . ' ' . $person->getFirstname() . ' ' . ' ' . $person->getMiddlename();
            if ($person->getPhones()) {
                $title .= ' (' . $person->getPhones() . ')';
            }
            $options[] = [
                'value' => $person->getRef(),
                'label' => $title,
            ];
        }
        return $options;
    }
}
