<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Config\Source;

use Mart\NovaPoshta\Api\AddressRepositoryInterface;

class City implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var AddressRepositoryInterface
     */
    private $_addressRepository;

    /**
     * City constructor.
     *
     * @param AddressRepositoryInterface $addressRepository
     */
    public function __construct(AddressRepositoryInterface $addressRepository)
    {
        $this->_addressRepository = $addressRepository;
    }

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        $options = [
            'value' => null,
            'label' => '',
        ];
        /** @var \Mart\NovaPoshta\Model\Address\City $city */
        foreach ($this->_addressRepository->getAllCities(true) as $city) {
            if (!isset($options[$city->getAreaRef()])) {
                $options[$city->getAreaRef()] = [
                    'value' => [],
                    'label' => $city->getAreaDescription(),
                ];
            }
            $options[$city->getAreaRef()]['value'][] = [
                'value' => $city->getRef(),
                'label' => $city->getDescription(),
            ];
        }
        return $options;
    }
}
