<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Config\Source;

class CargoType extends AbstractDirectory
{
    /**
     * @var array
     */
    protected $defaultTypes = [
        [
            'value' => 'Cargo',
            'label' => 'Вантаж',
        ],
        [
            'value' => 'Documents',
            'label' => 'Документи',
        ],
        [
            'value' => 'Pallet',
            'label' => 'Палети',
        ],
        [
            'value' => 'Parcel',
            'label' => 'Посилка',
        ],
        [
            'value' => 'TiresWheels',
            'label' => 'Шини-диски',
        ],
    ];

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        /** @var \Mart\NovaPoshta\Model\Directory\CargoType $serviceType */
        foreach ($this->_directoryRepository->getAllCargoTypes() as $serviceType) {
            $options[] = [
                'value' => $serviceType->getRef(),
                'label' => $serviceType->getDescription(),
            ];
        }

        return empty($options) ? $this->defaultTypes : $options;
    }
}
