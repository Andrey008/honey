<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Config\Backend;

use Magento\Framework\App\Config\Value;

class CitySender extends Value
{
    public function beforeSave()
    {
        $value = $this->getValue();
        $this->_registry->register('city_sender_ref', $value);
    }
}
