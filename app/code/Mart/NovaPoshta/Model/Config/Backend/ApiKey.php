<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Config\Backend;

use Mart\NovaPoshta\Api\CounterpartyManagementInterfaceFactory;
use Magento\Config\Model\Config\Backend\Encrypted;

/**
 * Class ApiKey
 * @package Mart\NovaPoshta\Model\Config\Backend
 * @method CounterpartyManagementInterfaceFactory getCounterpartyManagementFactory
 */
class ApiKey extends Encrypted
{
    /**
     * @return $this|Encrypted
     * @throws \Exception
     */
    public function afterSave()
    {
        parent::afterSave();
        try {
            $key = $this->processValue($this->getValue());
            $this->getCounterpartyManagementFactory()->create(['apiKey' => $key])->updateAll();
        } catch (\Exception $e) {
            throw new \Exception(__('Api Key error: %1', $e->getMessage()));
        }
        return $this;
    }
}
