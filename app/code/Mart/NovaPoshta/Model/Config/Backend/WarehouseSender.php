<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Config\Backend;

use Mart\NovaPoshta\Api\AddressRepositoryInterface;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Exception\NoSuchEntityException;

class WarehouseSender extends Value
{
    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * @var ConfigInterface
     */
    protected $configManager;

    /**
     * Methods constructor.
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Backend\Model\Session $session
     * @param \Magento\Framework\Math\Random $mathRandom
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Backend\Model\Session $session,
        AddressRepositoryInterface $addressRepository,
        ConfigInterface $configManager,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
        $this->configManager = $configManager;
        $this->addressRepository = $addressRepository;
    }
    public function beforeSave()
    {
        $value = $this->getValue();
        if (!empty($value) && is_numeric($value)) {
            try {
                $cityRef = $this->_registry->registry('city_sender_ref')
                    ?: $this->_config->getValue('carriers/novaposhta/city_sender');
                $warehouse = $this->addressRepository->getWarehouseByCityAndNumber($cityRef, $value);
                $value = $warehouse->getDescription();
                $this->configManager->saveConfig(
                    'carriers/novaposhta/warehouse_sender_ref',
                    $warehouse->getRef(),
                    $this->getScope(),
                    $this->getScopeId()
                );
            } catch (NoSuchEntityException $e) {
                $value = '';
            }
        }
        $this->setValue($value);
    }
}
