<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Config\Backend;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Exception\LocalizedException;

class Methods extends Value
{
    /**
     * @var \Magento\Framework\Math\Random
     */
    protected $mathRandom;

    /**
     * Methods constructor.
     *
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Backend\Model\Session $session
     * @param \Magento\Framework\Math\Random $mathRandom
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Backend\Model\Session $session,
        \Magento\Framework\Math\Random $mathRandom,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
        $this->mathRandom = $mathRandom;
    }

    /**
     * Process data after load
     *
     * @return void
     */
    protected function _afterLoad()
    {
        $value = $this->getValue();
        $value = $this->unserializeValue($value);
        $this->setValue($value);
    }

    /**
     * @throws LocalizedException
     */
    public function beforeSave()
    {
        $value = $this->getValue();
        if ((array)$value === $value) {
            $value = $this->serializeValue($value);
        }
        $this->setValue($value);
    }

    public function serializeValue(array $value)
    {
        unset($value['__empty']);
        $result = [];
        foreach ($value as $row) {
            if (isset($row['ref'])) {
                $row['order'] = (int)$row['order'];
                $row['free_shipping_subtotal'] = empty($row['free_shipping_subtotal'])
                    ? ''
                    :(float)$row['free_shipping_subtotal'];
                $result[$row['ref']] = $row;
            }
        }
        return \Zend_Json::encode($result);
    }

    public function unserializeValue($value)
    {
        $result = [];
        try {
            $value = \Zend_Json::decode($value);
            foreach ($value as $item) {
                $id = $this->mathRandom->getUniqueHash('_');
                $result[$id] = $item;
            }
        } catch (\Zend_Json_Exception $e) {
            $result = [];
        }
        return $result;
    }
}
