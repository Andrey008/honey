<?php

namespace Mart\NovaPoshta\Model\Gateway;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Phrase;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

class NovaPoshtaApi2
{
    /**
     * Key for API NovaPoshta
     */
    protected $key;

    /**
     * @var bool $throwErrors Throw exceptions when in response is error
     */
    protected $throwErrors;

    /**
     * @var string $format Format of returned data - array, json, xml
     */
    protected $format = 'array';

    /**
     * @var string $language Language of response
     */
    protected $language = 'ru';

    /**
     * @var string $connectionType Connection type (curl | file_get_contents)
     */
    protected $connectionType = 'curl';

    /**
     * @var Curl
     */
    private $curl;

    /**
     * @var LoggerInterface
     */
    private $_logger;

    /**
     * @var string $areas Areas (loaded from file, because there is no so function in NovaPoshta API 2.0)
     */
    protected $areas;

    /**
     * @var string $model Set current model for methods save(), update(), delete()
     */
    protected $model = 'Directory';

    /**
     * @var string $method Set method of current model
     */
    protected $method;

    /**
     * @var array $params Set params of current method of current model
     */
    protected $params;

    /**
     * NovaPoshtaApi2 constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param Curl $curl
     * @param LoggerInterface $logger
     * @param bool $throwErrors
     * @param string $connectionType
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Curl $curl,
        LoggerInterface $logger,
        $throwErrors = true,
        $connectionType = 'curl'
    ) {
        $key = $scopeConfig->getValue('carriers/novaposhta/api_key', ScopeInterface::SCOPE_STORE);
        $language = $scopeConfig->getValue('carriers/novaposhta/language', ScopeInterface::SCOPE_STORE);
        $this->curl = $curl;
        $this->_logger = $logger;
        $this->throwErrors = $throwErrors;
        return $this
            ->setKey($key)
            ->setLanguage($language)
            ->setConnectionType($connectionType)
            ->model('Directory');
    }

    /**
     * Setter for key property
     *
     * @param string $key NovaPoshta API key
     * @return NovaPoshtaApi2
     */
    public function setKey($key) {
        $this->key = $key;
        return $this;
    }

    /**
     * Getter for key property
     *
     * @return string
     */
    public function getKey() {
        return $this->key;
    }

    /**
     * Setter for $connectionType property
     *
     * @param string $connectionType Connection type (curl | file_get_contents)
     * @return $this
     */
    protected function setConnectionType($connectionType) {
        $this->connectionType = $connectionType;
        return $this;
    }

    /**
     * Getter for $connectionType property
     *
     * @return string
     */
    public function getConnectionType() {
        return $this->connectionType;
    }

    /**
     * Setter for language property
     *
     * @param string $language
     * @return NovaPoshtaApi2
     */
    protected function setLanguage($language) {
        $this->language = $language;
        return $this;
    }

    /**
     * Getter for language property
     *
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * Setter for format property
     *
     * @param string $format Format of returned data by methods (json, xml, array)
     * @return NovaPoshtaApi2
     */
    protected function setFormat($format) {
        $this->format = $format;
        return $this;
    }

    /**
     * Getter for format property
     *
     * @return string
     */
    public function getFormat() {
        return $this->format;
    }

    /**
     * Prepare data before return it
     * @param $data
     * @return array|mixed
     * @throws \Exception
     */
    private function prepare($data) {
        //Returns array
        if ($this->format == 'array') {
            $result = (array)$data === $data
                ? $data
                : \Zend_Json::decode($data);
            // If error exists, throw Exception
            if (count($result['errors'])) {
                $message = (array)$result['errors'] === $result['errors'] ? implode(". ", $result['errors']) : $result['errors'];
                $this->_logger->error($message);
                if ($this->throwErrors)
                    throw new LocalizedException(new Phrase($message));
            }
            return $result;
        }
        // Returns json or xml document
        return $data;
    }

    /**
     * Converts array to xml
     *
     * @param array
     */
    private function array2xml(array $array, $xml = false){
        ($xml === false) && $xml = new \SimpleXMLElement('<root/>');
        foreach($array as $key => $value){
            if (is_array($value)){
                $this->array2xml($value, $xml->addChild($key));
            } else {
                $xml->addChild($key, $value);
            }
        }
        return $xml->asXML();
    }

    /**
     * Make request to NovaPoshta API
     *
     * @param string $model Model name
     * @param string $method Method name
     * @param array $params Required params
     */
    private function request($model, $method, $params = null) {
        // Get required URL
        $url = $this->format == 'xml'
            ? 'https://api.novaposhta.ua/v2.0/xml/'
            : 'https://api.novaposhta.ua/v2.0/json/';

        $data = array(
            'apiKey' => $this->key,
            'modelName' => $model,
            'calledMethod' => $method,
            'language' => $this->language,
            'methodProperties' => $params
        );
        // Convert data to neccessary format
        $post = $this->format == 'xml'
            ? $this->array2xml($data)
            : $post = json_encode($data);

        if ($this->getConnectionType() == 'curl') {
            $this->curl->setOption(CURLOPT_SSL_VERIFYHOST, 0);
            $this->curl->setOption(CURLOPT_SSL_VERIFYPEER, 0);
            $this->curl->addHeader('Content-Type', $this->format == 'xml' ? 'text/xml' : 'application/json');
            $this->curl->post($url, $post);
            $result = $this->curl->getBody();
        } else {
            $result = file_get_contents($url, null, stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => "Content-type: application/x-www-form-urlencoded;\r\n",
                    'content' => $post,
                ),
            )));
        }

        return $this->prepare($result);
    }
    /**
     * Set current model and empties method and params properties
     *
     * @param string $model
     * @return mixed
     */
    protected function model($model = '') {
        if (!$model)
            return $this->model;
        $this->model = $model;
        $this->method = null;
        $this->params = null;
        return $this;
    }
    /**
     * Set method of current model property and empties params properties
     *
     * @param string $method
     * @return mixed
     */
    protected function method($method = '') {
        if (!$method)
            return $this->method;
        $this->method = $method;
        $this->params = null;
        return $this;
    }

    /**
     * Set params of current method/property property
     *
     * @param array $params
     * @return mixed
     */
    protected function params($params) {
        $this->params = $params;
        return $this;
    }

    /**
     * Execute request to NovaPoshta API
     *
     * @return mixed
     */
    public function execute() {
        return $this->request($this->model, $this->method, $this->params);
    }

    /**
     * Get tracking information by track number
     *
     * @param string $track Track number
     * @return mixed
     */
    public function documentsTracking($track) {
        return $this->request('TrackingDocument', 'getStatusDocuments', array('Documents' => array($track)));
    }

    /**
     * Get cities of company NovaPoshta
     *
     * @param int $page Num of page
     * @param string $findByString Find city by russian or ukrainian word
     * @param string $ref ID of city
     * @return mixed
     */
    public function getCities($page = 0, $findByString = '', $ref = '') {
        return $this->request('Address', 'getCities', array(
            'Page' => $page,
            'FindByString' => $findByString,
            'Ref' => $ref,
        ));
    }

    /**
     * Get warehouses by city
     *
     * @param string $cityRef ID of city
     * @param int $page
     * @return mixed
     */
    public function getWarehouses($cityRef = '', $page = 0) {
        return $this->request('Address', 'getWarehouses', array(
            'CityRef' => $cityRef,
            'Page' => $page,
        ));
    }


    /**
     * Get warehouse types
     * @return array|mixed
     */
    public function getWarehouseTypes()
    {
        return $this->request('Address', 'getWarehouseTypes');
    }

    /**
     * Get 5 nearest warehouses by array of strings
     *
     * @param array $searchStringArray
     * @return mixed
     */
    public function findNearestWarehouse($searchStringArray) {
        $searchStringArray = (array) $searchStringArray;
        return $this->request('Address', 'findNearestWarehouse', array(
            'SearchStringArray' => $searchStringArray,
        ));
    }

    /**
     * Get streets list by city and/or search string
     *
     * @param string $cityRef ID of city
     * @param string $findByString
     * @param int $page
     * @return mixed
     */
    public function getStreet($cityRef, $findByString = '', $page = 0) {
        return $this->request('Address', 'getStreet', array(
            'FindByString' => $findByString,
            'CityRef' => $cityRef,
            'Page' => $page,
        ));
    }

    /**
     * Find current area in list of areas
     *
     * @param array $areas List of arias, getted from file
     * @param string $findByString Area name
     * @param string $ref Area Ref ID
     * @return array
     */
    protected function findArea(array $areas, $findByString = '', $ref = '') {
        $data = array();
        if (!$findByString && !$ref)
            return $data;
        // Try to find current region
        foreach ($areas as $key => $area) {
            // Is current area found by string or by key
            $found = $findByString
                ? ((mb_stripos($area['Description'], $findByString) !== false)
                    || (mb_stripos($area['DescriptionRu'], $findByString) !== false)
                    || (mb_stripos($area['Area'], $findByString) !== false)
                    || (mb_stripos($area['AreaRu'], $findByString) !== false))
                : ($key == $ref);
            if ($found) {
                $area['Ref'] = $key;
                $data[] = $area;
                break;
            }
        }
        return $data;
    }

    /**
     * Get areas list by city and/or search string
     *
     * @param string $ref ID of area
     * @param int $page
     * @return mixed
     */
    public function getAreas($ref = '', $page = 0) {
        return $this->request('Address', 'getAreas', array(
            'Ref' => $ref,
            'Page' => $page,
        ));
    }

    /**
     * Magic method of calling functions (uses for calling Common Model of NovaPoshta API)
     *
     * @param string $method Called method of Directory Model
     * @param array $arguments Array of params
     */
    public function __call($method, $arguments) {
        $directoryModelMethod = array(
            'getTypesOfCounterparties',
            'getBackwardDeliveryCargoTypes',
            'getCargoDescriptionList',
            'getCargoTypes',
            'getDocumentStatuses',
            'getOwnershipFormsList',
            'getPalletsList',
            'getPaymentForms',
            'getTimeIntervals',
            'getServiceTypes',
            'getTiresWheelsList',
            'getTraysList',
            'getTypesOfAlternativePayers',
            'getTypesOfPayers',
            'getTypesOfPayersForRedelivery',
        );
        // Call method of Common model
        if (in_array($method, $directoryModelMethod)) {
            return $this
                ->model('Common')
                ->method($method)
                ->params(null)
                ->execute();
        }
    }

    /**
     * Delete method of current model
     *
     * @param array $params
     * @return mixed
     */
    public function delete($params) {
        return $this->request($this->model, 'delete', $params);
    }

    /**
     * Update method of current model
     * Required params:
     * For ContactPerson model: Ref, CounterpartyRef, FirstName (ukr), MiddleName, LastName, Phone (format 0xxxxxxxxx)
     * For Counterparty model: Ref, CounterpartyProperty (Recipient|Sender), CityRef, CounterpartyType (Organization, PrivatePerson),
     * FirstName (or name of organization), MiddleName, LastName, Phone (0xxxxxxxxx), OwnershipForm (if Organization)
     *
     * @param array $params
     * @return mixed
     */
    public function update($params) {
        return $this->request($this->model, 'update', $params);
    }

    /**
     * Save method of current model
     * Required params:
     * For ContactPerson model (only for Organization API key, for PrivatePerson error will be returned):
     *	 CounterpartyRef, FirstName (ukr), MiddleName, LastName, Phone (format 0xxxxxxxxx)
     * For Counterparty model:
     *	 CounterpartyProperty (Recipient|Sender), CityRef, CounterpartyType (Organization, PrivatePerson),
     *	 FirstName (or name of organization), MiddleName, LastName, Phone (0xxxxxxxxx), OwnershipForm (if Organization)
     *
     * @param array $params
     * @return mixed
     */
    public function save($params) {
        return $this->request($this->model, 'save', $params);
    }

    /**
     * getCounterparties() function of model Counterparty
     *
     * @param string $counterpartyProperty Type of Counterparty (Sender|Recipient)
     * @param int $page Page number
     * @param string $findByString String to search
     * @param string $cityRef City ID
     * @return mixed
     */
    public function getCounterparties($counterpartyProperty = 'Recipient', $page = null, $findByString = null, $cityRef = null) {
        // Any param can be skipped
        $params = array();
        $params['CounterpartyProperty'] = $counterpartyProperty ? $counterpartyProperty : 'Recipient';
        $page && $params['Page'] = $page;
        $findByString && $params['FindByString'] = $findByString;
        $cityRef && $params['CityRef'] = $cityRef;
        return $this->request('Counterparty', 'getCounterparties', $params);
    }

    /**
     * cloneLoyaltyCounterpartySender() function of model Counterparty
     * The counterparty will be not created immediately, you can wait a long time
     *
     * @param string $cityRef City ID
     * @return mixed
     */
    public function cloneLoyaltyCounterpartySender($cityRef) {
        return $this->request('Counterparty', 'cloneLoyaltyCounterpartySender', array('CityRef' => $cityRef));
    }

    /**
     * getCounterpartyContactPersons() function of model Counterparty
     *
     * @param string $ref Counterparty ref
     * @return mixed
     */
    public function getCounterpartyContactPersons($ref) {
        return $this->request('Counterparty', 'getCounterpartyContactPersons', array('Ref' => $ref));
    }

    /**
     * getCounterpartyAddresses() function of model Counterparty
     *
     * @param string $ref Counterparty ref
     * @param int $page
     * @return mixed
     */
    public function getCounterpartyAddresses($ref, $page = 0) {
        return $this->request('Counterparty', 'getCounterpartyAddresses', array('Ref' => $ref, 'Page' => $page));
    }

    /**
     * getCounterpartyOptions() function of model Counterparty
     *
     * @param string $ref Counterparty ref
     * @return mixed
     */
    public function getCounterpartyOptions($ref) {
        return $this->request('Counterparty', 'getCounterpartyOptions', array('Ref' => $ref));
    }

    /**
     * getCounterpartyByEDRPOU() function of model Counterparty
     *
     * @param string $edrpou EDRPOU code
     * @param string $cityRef City ID
     * @return mixed
     */
    public function getCounterpartyByEDRPOU($edrpou, $cityRef) {
        return $this->request('Counterparty', 'getCounterpartyByEDRPOU', array('EDRPOU' => $edrpou, 'cityRef' => $cityRef));
    }

    /**
     * @param $citySender
     * @param $cityRecipient
     * @param $serviceType
     * @param $weight
     * @param $cost
     * @param array $volumes
     * @param null $cargoType
     * @return array|mixed
     */
    public function getDocumentPrice(
        $citySender,
        $cityRecipient,
        $serviceType,
        $weight,
        $cost,
        array $volumes = [],
        $cargoType = null
    ) {

        return $this->request('InternetDocument', 'getDocumentPrice', array(
            'CitySender' => $citySender,
            'CityRecipient' => $cityRecipient,
            'ServiceType' => $serviceType,
            'Weight' => $weight,
            'Cost' => $cost,
            'CargoType' => $cargoType,
            'OptionsSeat' => $volumes
        ));
    }
    /**
     * Get approximately date of delivery between two cities
     *
     * @param string $citySender City ID
     * @param string $cityRecipient City ID
     * @param string $serviceType (DoorsDoors|DoorsWarehouse|WarehouseWarehouse|WarehouseDoors)
     * @param string|null $dateTime Date of shipping
     * @return mixed
     */
    public function getDocumentDeliveryDate($citySender, $cityRecipient, $serviceType, $dateTime = null) {
        return $this->request('InternetDocument', 'getDocumentDeliveryDate', array(
            'CitySender' => $citySender,
            'CityRecipient' => $cityRecipient,
            'ServiceType' => $serviceType,
            'DateTime' => $dateTime,
        ));
    }

    /**
     * Get documents list
     *
     * @param array $params List of params
     * Not required keys:
     * 'Ref', 'IntDocNumber', 'InfoRegClientBarcodes', 'DeliveryDateTime', 'RecipientDateTime',
     * 'CreateTime', 'SenderRef', 'RecipientRef', 'WeightFrom', 'WeightTo',
     * 'CostFrom', 'CostTo', 'SeatsAmountFrom', 'SeatsAmountTo', 'CostOnSiteFrom',
     * 'CostOnSiteTo', 'StateIds', 'ScanSheetRef', 'DateTime', 'DateTimeFrom',
     * 'RecipientDateTime', 'isAfterpayment', 'Page', 'OrderField =>
     *   [
     *    IntDocNumber, DateTime, Weight, Cost, SeatsAmount, CostOnSite,
     *    CreateTime, EstimatedDeliveryDate, StateId, InfoRegClientBarcodes, RecipientDateTime
     *   ],
     * 'OrderDirection' => [DESC, ASC], 'ScanSheetRef'
     * @return mixed
     */
    public function getDocumentList($params = null) {
        return $this->request('InternetDocument', 'getDocumentList', $params ? $params : null);
    }

    /**
     * Get document info by ID
     *
     * @param string $ref Document ID
     * @return mixed
     */
    public function getDocument($ref) {
        return $this->request('InternetDocument', 'getDocument', array(
            'Ref' => $ref,
        ));
    }

    /**
     * Generetes report by Document refs
     *
     * @param array $params Params like getDocumentList with requiered keys
     *  'Type' => [xls, csv], 'DocumentRefs' => []
     * @return mixed
     */
    public function generateReport($params) {
        return $this->request('InternetDocument', 'generateReport', $params);
    }

    /**
     * Check required fields for new InternetDocument and set defaults
     *
     * @param array $counterparty
     * @throws \Exception
     */
    protected function checkInternetDocumentRecipient(array &$counterparty) {
        // Check required fields
        if (!$counterparty['FirstName'])
            throw new \Exception('FirstName is required filed for recipient');
        // MiddleName realy is not required field, but manual says otherwise
        // if (!$counterparty['MiddleName'])
        // throw new \Exception('MiddleName is required filed for sender and recipient');
        if (!$counterparty['LastName'])
            throw new \Exception('LastName is required filed for recipient');
        if (!$counterparty['Phone'])
            throw new \Exception('Phone is required filed for recipient');
        if (!($counterparty['City'] || $counterparty['CityRef']))
            throw new \Exception('City is required filed for recipient');
//        if (!($counterparty['Region'] || $counterparty['CityRef']))
//            throw new \Exception('Region is required filed for recipient');

        // Set defaults
        if (!$counterparty['CounterpartyType']) {
            $counterparty['CounterpartyType'] = 'PrivatePerson';
        }
    }

    /**
     * Check required params for new InternetDocument and set defaults
     *
     * @param array $params
     * @throws \Exception
     */
    protected function checkInternetDocumentParams(array &$params) {
        if (!$params['Description'])
            throw new \Exception('Description is required filed for new Internet document');
        if (!$params['Weight'])
            throw new \Exception('Weight is required filed for new Internet document');
        if (!$params['Cost'])
            throw new \Exception('Cost is required filed for new Internet document');
        (!$params['DateTime']) && $params['DateTime'] = date('d.m.Y');
        (!$params['ServiceType']) && $params['ServiceType'] = 'WarehouseWarehouse';
        (!$params['PaymentMethod']) && $params['PaymentMethod'] = 'Cash';
        (!$params['PayerType']) && $params['PayerType'] = 'Recipient';
        (!$params['SeatsAmount']) && $params['SeatsAmount'] = '1';
        (!$params['CargoType']) && $params['CargoType'] = 'Cargo';
        (!$params['VolumeGeneral']) && $params['VolumeGeneral'] = '0.0004';
    }

    /**
     * Create Internet Document by
     *
     * @param array $sender Sender info.
     * 	Required:
     *  For existing sender:
     *	  'Description' => String (Full name i.e.), 'City' => String (City name)
     *  For creating:
     * 		'FirstName' => String, 'MiddleName' => String,
     * 		'LastName' => String, 'Phone' => '000xxxxxxx', 'City' => String (City name), 'Region' => String (Region name),
     * 		'Warehouse' => String (Description from getWarehouses))
     * @param array $recipient Recipient info, same like $sender param
     * @param array $params Additional params of Internet Document
     *  Required:
     * 		'Description' => String, 'Weight' => Float, 'Cost' => Float
     *  Recommended:
     * 		'VolumeGeneral' => Float (default = 0.004), 'SeatsAmount' => Int (default = 1),
     * 		'PayerType' => (Sender|Recipient - default), 'PaymentMethod' => (NonCash|Cash - default)
     * 		'ServiceType' => (DoorsDoors|DoorsWarehouse|WarehouseDoors|WarehouseWarehouse - default)
     * 		'CargoType' => String
     * @param mixed
     */
    public function newInternetDocument($sender, $recipient, $params) {
        // Check for required params and set defaults
        $this->checkInternetDocumentRecipient($recipient);
        $this->checkInternetDocumentParams($params);

        // Prepare recipient data
        $recipient['CounterpartyProperty'] = 'Recipient';
        $recipient['CityRef'] = $recipient['CityRecipient'];
        if (!$recipient['Recipient']) {
            $recipientCounterparty = $this->model('Counterparty')->save($recipient);
            $recipient['Recipient'] = $recipientCounterparty['data'][0]['Ref'];
            $recipient['ContactRecipient'] = $recipientCounterparty['data'][0]['ContactPerson']['data'][0]['Ref'];
        }
        // Full params is merge of arrays $sender, $recipient, $params
        $paramsInternetDocument = array_merge($sender, $recipient, $params);
        // Creating new Internet Document
        return $this->model('InternetDocument')->save($paramsInternetDocument);
    }

    public function deleteInternetDocument($ref)
    {
        return $this->model('InternetDocument')->delete(['DocumentRefs' => [$ref]]);
    }
    /**
     * Get only link on internet document for printing
     *
     * @param string $method Called method of NovaPoshta API
     * @param array|string $documentRefs Array of Documents IDs
     * @param string $type (html_link|pdf_link)
     * @return mixed
     */
    protected function printGetLink($method, $documentRefs, $type) {
        $data = 'https://my.novaposhta.ua/orders/' . $method . '/orders[]/'.implode(',', $documentRefs)
            .'/type/'.str_replace('_link', '', $type)
            .'/apiKey/'.$this->key;
        // Return data in same format like NovaPoshta API
        return $this->prepare(
            array(
                'success' => true,
                'data' => array($data),
                'errors' => array(),
                'warnings' => array(),
                'info' => array(),
            ));
    }
    /**
     * printDocument method of InternetDocument model
     *
     * @param array|string $documentRefs Array of Documents IDs
     * @param string $type (pdf|html|html_link|pdf_link)
     * @return mixed
     */
    public function printDocument($documentRefs, $type = 'html') {
        $documentRefs = (array) $documentRefs;
        // If needs link
        if ($type == 'html_link' || $type == 'pdf_link')
            return $this->printGetLink('printDocument', $documentRefs, $type);
        // If needs data
        return $this->request('InternetDocument', 'printDocument', array('DocumentRefs' => $documentRefs, 'Type' => $type));
    }
    /**
     * printMarkings method of InternetDocument model
     *
     * @param array|string $documentRefs Array of Documents IDs
     * @param string $type (pdf|new_pdf|new_html|old_html|html_link|pdf_link)
     * @return mixed
     */
    public function printMarkings($documentRefs, $type = 'new_html') {
        $documentRefs = (array) $documentRefs;
        // If needs link
        if ($type == 'html_link' || $type == 'pdf_link')
            return $this->printGetLink('printMarkings', $documentRefs, $type);
        // If needs data
        return $this->request('InternetDocument', 'printMarkings', array('DocumentRefs' => $documentRefs, 'Type' => $type));
    }
}
