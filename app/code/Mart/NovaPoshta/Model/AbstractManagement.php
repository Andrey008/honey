<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model;

use Mart\NovaPoshta\Model\Gateway\NovaPoshtaApi2;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Filter\Translit;
use Psr\Log\LoggerInterface;

abstract class AbstractManagement
{
    /**
     * @var NovaPoshtaApi2
     */
    protected $_api;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $resourceConnection;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * AbstractManagement constructor.
     *
     * @param NovaPoshtaApi2 $api
     * @param ResourceConnection $resourceConnection
     * @param LoggerInterface $logger
     * @param null|string $apiKey
     */
    public function __construct(
        NovaPoshtaApi2 $api,
        ResourceConnection $resourceConnection,
        Loggerinterface $logger,
        $apiKey = null
    ) {
        $this->_api = $api;
        if ($apiKey) {
            $this->_api->setKey($apiKey);
        }
        $this->resourceConnection = $resourceConnection;
        $this->logger = $logger;
    }

    protected function _updateEntity(array $source, $table, array $map, array $fields = [])
    {
        $error = false;
        try {
            $table = $this->resourceConnection->getTableName($table);
            foreach ($source['data'] as $item) {
                $data = [];
                foreach ($map as $column => $field) {
                    $data[$column] = is_callable($field)
                        ? call_user_func($field, $item)
                        : ($item[$field] ?? '');
                }
                $this->resourceConnection->getConnection()->insertOnDuplicate(
                    $table,
                    $data,
                    $fields
                );
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            $error = $e->getMessage();
        }
        return $error;
    }
}
