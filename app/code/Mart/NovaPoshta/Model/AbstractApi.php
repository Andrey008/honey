<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model;

use Mart\NovaPoshta\Helper\Config;
use Mart\NovaPoshta\Model\Gateway\NovaPoshtaApi2;
use Psr\Log\LoggerInterface;

class AbstractApi
{
    /**
     * @var NovaPoshtaApi2
     */
    protected $_api;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * AbstractApi constructor.
     *
     * @param NovaPoshtaApi2 $api
     * @param Config $config
     */
    public function __construct(
        NovaPoshtaApi2 $api,
        Config $config,
        LoggerInterface $logger
    ) {
        $this->_api = $api;
        $this->config = $config;
        $this->logger = $logger;
    }
}
