<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\ResourceModel\Counterparty;

class Counterparty extends \Mart\NovaPoshta\Model\ResourceModel\AbstractResource
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('np_counterparty', 'entity_id');
    }
}
