<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\ResourceModel\Address\City;

use Mart\NovaPoshta\Model\Address\Area;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize collection model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Mart\NovaPoshta\Model\Address\City::class,
            \Mart\NovaPoshta\Model\ResourceModel\Address\City::class
        );
        parent::_construct();
    }

    public function addAreaNames()
    {
        $this->join(
            ['area' => Area::TABLE_NAME],
            'main_table.area_ref = area.ref',
            ['area_description' => 'description']
        );
    }

    /**
     * @return array
     */
   public function toOptionArray()
    {
        $valueField = 'ref';
        $labelField = 'description';
        $additional = [
            'area_ref' => 'area_ref'
        ];
        return $this->_toOptionArray($valueField, $labelField, $additional);
    }
}
