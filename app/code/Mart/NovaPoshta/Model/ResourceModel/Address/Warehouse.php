<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\ResourceModel\Address;

class Warehouse extends \Mart\NovaPoshta\Model\ResourceModel\AbstractResource
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('np_warehouse', 'entity_id');
    }

    /**
     * @param $ref
     * @param $number
     */
    public function getWarehouseIdByCityAndNumber($cityRef, $number)
    {
        $connection = $this->getConnection();

        $select = $connection->select()
            ->from($this->getMainTable(), $this->_idFieldName)
            ->where('city_ref = :city_ref AND number = :number');

        $bind = [
            ':city_ref' => (string)$cityRef,
            ':number' => (int)$number
        ];

        return $connection->fetchOne($select, $bind);
    }
}
