<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\ResourceModel\Address\Warehouse;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize collection model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Mart\NovaPoshta\Model\Address\Warehouse::class,
            \Mart\NovaPoshta\Model\ResourceModel\Address\Warehouse::class
        );
        parent::_construct();
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $valueField = 'ref';
        $labelField = 'description';
        $additional = [
            'city_ref' => 'city_ref',
            'max_weight' => 'max_weight',
            'place_max_weight' => 'place_max_weight',
        ];
        return parent::_toOptionArray($valueField, $labelField, $additional);
    }
}
