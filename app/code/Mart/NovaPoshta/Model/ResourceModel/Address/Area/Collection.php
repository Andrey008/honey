<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\ResourceModel\Address\Area;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize collection model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Mart\NovaPoshta\Model\Address\Area::class,
            \Mart\NovaPoshta\Model\ResourceModel\Address\Area::class
        );
        parent::_construct();
    }

    /**
     * @param string $valueField
     * @param string $labelField
     * @param array $additional
     * @return array
     */
    protected function _toOptionArray($valueField = 'ref', $labelField = 'description', $additional = [])
    {
        return parent::_toOptionArray($valueField, $labelField, $additional);
    }
}
