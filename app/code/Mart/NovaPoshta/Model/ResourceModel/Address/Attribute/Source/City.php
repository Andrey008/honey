<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\ResourceModel\Address\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\Table;

class City extends Table
{
    /**
     * @var \Mart\NovaPoshta\Model\ResourceModel\Address\City\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * City constructor.
     *
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory
     * @param \Mart\NovaPoshta\Model\ResourceModel\Address\City\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory,
        \Mart\NovaPoshta\Model\ResourceModel\Address\City\CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($attrOptionCollectionFactory, $attrOptionFactory);
    }

    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        if (!isset($this->_options)) {
            $this->_options = $this->_collectionFactory->create()->toOptionArray();
        }
        return $this->_options;
    }
}
