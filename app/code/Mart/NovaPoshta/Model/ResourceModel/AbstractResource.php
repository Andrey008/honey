<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\ResourceModel;

abstract class AbstractResource extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @param $ref
     * @return string
     */
    public function getIdByRef($ref)
    {
        $connection = $this->getConnection();

        $select = $connection->select()
            ->from($this->getMainTable(), $this->_idFieldName)
            ->where('ref = :ref');

        $bind = [':ref' => (string)$ref];

        return $connection->fetchOne($select, $bind);
    }
}
