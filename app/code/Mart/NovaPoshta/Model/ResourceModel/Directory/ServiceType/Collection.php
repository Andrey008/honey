<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\ResourceModel\Directory\ServiceType;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize collection model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Mart\NovaPoshta\Model\Directory\ServiceType::class,
            \Mart\NovaPoshta\Model\ResourceModel\Directory\ServiceType::class
        );
        parent::_construct();
    }
}
