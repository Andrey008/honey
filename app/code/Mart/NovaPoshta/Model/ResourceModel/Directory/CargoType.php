<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\ResourceModel\Directory;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class CargoType extends AbstractDb
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('np_cargo_type', 'ref');
    }
}
