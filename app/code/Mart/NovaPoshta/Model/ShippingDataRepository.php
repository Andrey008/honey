<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model;

use Mart\NovaPoshta\Api\ShippingDataRepositoryInterface;
use Magento\Framework\App\ResourceConnection;
use Psr\Log\LoggerInterface;

class ShippingDataRepository extends AbstractManagement implements ShippingDataRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function save($quoteId, array $data)
    {
        try {
            $this->resourceConnection->getConnection()->insertOnDuplicate(
                $this->resourceConnection->getTableName(self::DATA_TABLE_NAME),
                [
                    'quote_id' => $quoteId,
                    'shipping_data' => \Zend_Json::encode($data)
                ],
                ['quote_id', 'shipping_data']
            );
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function get($quoteId)
    {
        $data = [];
        try {
            $select = $this->resourceConnection
                ->getConnection()
                ->select()
                ->from($this->resourceConnection->getTableName(self::DATA_TABLE_NAME), ['shipping_data'])
                ->where('quote_id = ?', $quoteId);
            $data = $this->resourceConnection->getConnection()->fetchOne($select);
            $data = \Zend_Json::decode($data);
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
        return $data;
    }
}
