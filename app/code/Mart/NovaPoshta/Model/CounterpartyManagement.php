<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model;

use Mart\NovaPoshta\Api\CounterpartyManagementInterface;
use Mart\NovaPoshta\Model\Counterparty\ContactPerson;
use Mart\NovaPoshta\Model\Counterparty\Counterparty;

class CounterpartyManagement extends AbstractManagement implements CounterpartyManagementInterface
{
    /**
     * @var array|null
     */
    protected $counterparties;

    /**
     * @inheritdoc
     */
    public function updateAll()
    {
        return [
            'Counterparties' => $this->updateCounterparties(),
            'Contact Persons' => $this->updateContactPersons(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function updateCounterparties()
    {
        $map = [
            'ref' => 'Ref',
            'description' => 'Description',
            'city' => 'City',
            'firstname' => 'FirstName',
            'lastname' => 'LastName',
            'middlename' => 'MiddleName',
            'ownership_form_ref' => 'OwnershipFormRef',
            'ownership_form_description' => 'OwnershipFormDescription',
            'edrpou' => 'EDRPOU',
            'counterparty_type' => 'CounterpartyType',
        ];
        $error = $this->_updateEntity($this->_getCounterpaties(), Counterparty::TABLE_NAME, $map);
        return $error;
    }

    /**
     * @inheritdoc
     */
    public function updateContactPersons()
    {
        $counterparties = $this->_getCounterpaties();
        if (!$counterparties) {
            $error = __('No counterparties received');
            $this->logger->notice($error);
            return $error;
        }
        $refs = [];
        foreach ($counterparties['data'] as $counterparty) {
            $refs[] = $counterparty['Ref'];
        }
        $error = false;
        foreach ($refs as $ref) {
            $contactPersons = $this->_api->getCounterpartyContactPersons($ref);
            $map = [
                'ref' => 'Ref',
                'counterparty_ref' => function () use ($ref) {return $ref;},
                'description' => 'Description',
                'phones' => 'Phones',
                'email' => 'Email',
                'firstname' => 'FirstName',
                'lastname' => 'LastName',
                'middlename' => 'MiddleName',
            ];
            $error = $this->_updateEntity($contactPersons, ContactPerson::TABLE_NAME, $map);
            if ($error) {
                break;
            }
        }
        return $error;
    }

    /**
     * @return array|mixed|null
     */
    protected function _getCounterpaties()
    {
        if (!isset($this->counterparties)) {
            $this->counterparties = $this->_api->getCounterparties('Sender');
        }
        return $this->counterparties;
    }
}
