<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Carrier;

use Mart\NovaPoshta\Api\CounterpartyRepositoryInterface;
use Mart\NovaPoshta\Model\Api\RequestFactory;
use Mart\NovaPoshta\Api\DirectoryRepositoryInterface;
use Mart\NovaPoshta\Api\ShippingDataRepositoryInterface;
use Magento\Framework\App\CacheInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Framework\Xml\Security;
use Mart\NovaPoshta\Api\InternetDocumentInterface;

/**
 * Class NovaPoshta
 * @package Mart\NovaPoshta\Model\Carrier
 */
class NovaPoshta extends \Magento\Shipping\Model\Carrier\AbstractCarrierOnline implements CarrierInterface
{
    const CODE = 'novaposhta';
    const NP_CURRENCY = 'UAH';
    const CACHE_LIFETIME = 86400;
    const DATA_PRICE = 'price';
    const DATA_ETA = 'eta';

    /**
     * Todo: use api to get this types
     * @var string
     */
    protected $payerType = 'Sender';
    protected $paymentMethod = 'Cash';
    protected $recipientType = 'PrivatePerson';

    /**
     * @var string
     */
    protected $dateFormat = 'd.m.Y';

    /**
     * @var array
     */
    protected $_apiMethods = [
        self::DATA_PRICE => 'getDocumentPrice',
        self::DATA_ETA => 'getDocumentDeliveryDate',
    ];

    /**
     * @var int|null
     */
    protected $_orderQuoteId;

    /**
     * @var string
     */
    protected $_code = self::CODE;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_localeDate;

    /**
     * @var RequestFactory
     */
    protected $_requestFactory;

    /**
     * @var InternetDocumentInterface
     */
    private $_internetDocument;

    /**
     * @var DirectoryRepositoryInterface
     */
    protected $_directoryRepository;

    /**
     * @var CacheInterface
     */
    protected $_cache;

    /**
     * @var ShippingDataRepositoryInterface
     */
    protected $_shippingDataRepository;

    /**
     * @var CounterpartyRepositoryInterface
     */
    protected $_counterpartyRepository;

    /**
     * NovaPoshta constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param Security $xmlSecurity
     * @param \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory
     * @param \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory
     * @param \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Directory\Helper\Data $directoryData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param RequestFactory $requestFactory
     * @param InternetDocumentInterface $internetDocument
     * @param DirectoryRepositoryInterface $directoryRepository
     * @param CacheInterface $cache
     * @param ShippingDataRepositoryInterface $shippingDataRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        Security $xmlSecurity,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        RequestFactory $requestFactory,
        InternetDocumentInterface $internetDocument,
        DirectoryRepositoryInterface $directoryRepository,
        CacheInterface $cache,
        ShippingDataRepositoryInterface $shippingDataRepository,
        CounterpartyRepositoryInterface $counterpartyRepository,
        array $data = []
    )
    {
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $data
        );
        $this->storeManager = $storeManager;
        $this->_localeDate = $localeDate;
        $this->_requestFactory = $requestFactory;
        $this->_internetDocument = $internetDocument;
        $this->_directoryRepository = $directoryRepository;
        $this->_cache = $cache;
        $this->_shippingDataRepository = $shippingDataRepository;
        $this->_counterpartyRepository = $counterpartyRepository;
    }

    /**
     * Processing additional validation to check if carrier applicable.
     *
     * @param \Magento\Framework\DataObject $request
     * @return $this|bool|\Magento\Framework\DataObject
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function proccessAdditionalValidation(\Magento\Framework\DataObject $request)
    {
        if (!$request instanceof RateRequest) {
            return $this;
        }
        //Skip by item validation if there is no items in request
        if (!count($this->getAllItems($request))) {
            return $this;
        }

        $maxAllowedWeight = (double)$this->getConfigData('max_package_weight');
        $errorMsg = '';
        $configErrorMsg = $this->getConfigData('specificerrmsg');
        $defaultErrorMsg = __('The shipping module is not available.');
        $showMethod = $this->getConfigData('showmethod');

        /** @var $item \Magento\Quote\Model\Quote\Item */
        foreach ($this->getAllItems($request) as $item) {
            $product = $item->getProduct();
            if ($product && $product->getId()) {
                $weight = $product->getWeight();
                $stockItemData = $this->stockRegistry->getStockItem(
                    $product->getId(),
                    $item->getStore()->getWebsiteId()
                );
                $doValidation = true;

                if ($stockItemData->getIsQtyDecimal() && $stockItemData->getIsDecimalDivided()) {
                    if ($stockItemData->getEnableQtyIncrements() && $stockItemData->getQtyIncrements()
                    ) {
                        $weight = $weight * $stockItemData->getQtyIncrements();
                    } else {
                        $doValidation = false;
                    }
                } elseif ($stockItemData->getIsQtyDecimal() && !$stockItemData->getIsDecimalDivided()) {
                    $weight = $weight * $item->getQty();
                }

                if ($doValidation && $weight > $maxAllowedWeight) {
                    $errorMsg = $configErrorMsg ? $configErrorMsg : $defaultErrorMsg;
                    break;
                }
            }
        }

        if ($errorMsg && $showMethod) {
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($errorMsg);

            return $error;
        } elseif ($errorMsg) {
            return false;
        }

        return $this;
    }

    /**
     * Check if carrier has shipping label option available
     *
     * @return boolean
     */
    public function isShippingLabelsAvailable()
    {
        return $this->getConfigFlag('create_eway');
    }

    /**
     * Do request to shipment
     *
     * @param \Magento\Shipping\Model\Shipment\Request $request
     * @return \Magento\Framework\DataObject
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function requestToShipment($request)
    {
        $citySender = $this->getConfigData('city_sender');
        $shipperRef = $this->getConfigData('shipper_ref');
        $request->setShipperAddressCity($citySender);
        $request->setShipperWarehouseRef($this->getConfigData('warehouse_sender_ref'));
        $request->setShipperRef($shipperRef);
        $request->setPayerType($this->payerType);
        $request->setPaymentMethod($this->paymentMethod);
        $request->setCargoType($this->getConfigData('cargo_type'));
        $request->setDateTime($this->_getSendDate());
        $order = $request->getOrderShipment()->getOrder();
        $this->_orderQuoteId = $order->getQuoteId();
        $request->setSender($this->_counterpartyRepository->getPersonByRef($this->getConfigData('shipper_ref')));
        $additionalData = $this->_shippingDataRepository->get($this->_orderQuoteId);
        $request->addData($additionalData);
        return parent::requestToShipment($request);
    }

    /**
     * @param \Mart\NovaPoshta\Model\Api\Request $request
     * @return \Magento\Framework\DataObject
     */
    protected function _doShipmentRequest(\Magento\Framework\DataObject $request)
    {
        $response = $this->_internetDocument->createDocument($request);
        $result = new \Magento\Framework\DataObject();
        foreach ($response['data'] as $data) {
            $info = [
                'tracking_number' => $data['IntDocNumber'],
                'shipping_label_content' => 'NovaPoshta'
            ];
            $this->_updateShippingData($data);
            return $result->addData($info);
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getAllowedMethods()
    {
        $methods = [];
        try {
            $allowedMethods = \Zend_Json::decode($this->getConfigData('allowed_methods'));
        } catch (\Zend_Json_Exception $e) {
            $this->_logger->warning($e->getMessage());
            $allowedMethods = [];
        }
        usort($allowedMethods, function ($a, $b) {
            return $a['order'] > $b['order'] ? 1 : -1;
        });
        foreach ($allowedMethods as $method) {
            $methods[$method['ref']] = $method;
        }
        return $methods;
    }

    public function collectRates(RateRequest $request)
    {
        $result = $this->_rateFactory->create();
        $items = $request->getAllItems();
        $quoteId = reset($items)->getQuoteId();
        $customAttributes = $this->_shippingDataRepository->get($quoteId);
        $currencyCode = $this->storeManager->getStore()->getBaseCurrencyCode();
        $volumes = [];
        foreach ($items as $item) {
            $volumes[] = [
                'volumetricLength' => $item->getProduct()->getTsDimensionsLength(),
                'volumetricWidth' => $item->getProduct()->getTsDimensionsWidth(),
                'volumetricHeight' => $item->getProduct()->getTsDimensionsHeight(),
            ];
        }
        $request->setVolumes($volumes);
        foreach ($this->getAllowedMethods() as $code => $data) {
            $method = $this->_rateMethodFactory->create();

            try {
                $title = $data['title'];
                if (empty($title)) {
                    $title = $this->_directoryRepository->getServiceType($code)->getDescription();
                }
                if ($this->getConfigData('shipping_calculation')) {
                    $price = $this->_calcData($request, $customAttributes, $code, self::DATA_PRICE)
                        ?: $this->getConfigData('price');
                } else {
                    $price = $this->getConfigData('price');
                }

                $price = $this->_getMethodPrice($price, $data, $request->getBaseSubtotalInclTax());
                if ($this->getConfigData('calc_eta')) {
                    $etaDate = $this->_calcData($request, $customAttributes, $code, self::DATA_ETA);
                    if ($etaDate) {
                        $etaDate = $this->_localeDate->formatDate($etaDate);
                        $title .= $etaDate ? ' ' . __('(ETA %1)', $etaDate) : '';
                    }
                }
            } catch (\Exception $e) {
                $this->_logger->error($e->getMessage());
                continue;
            }

            $method->setCarrier(self::CODE);
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod($code);
            $method->setMethodTitle($title);

            if ($currencyCode !== self::NP_CURRENCY) {
                try {
                    $currency = $this->_currencyFactory->create()->load(self::NP_CURRENCY);
                    $price = $currency->convert($price, $currencyCode);
                } catch (\Exception $e) {
                    // Rate for UAH is not set
                }
            }
            $method->setPrice($price);
            $method->setCost($price);
            $result->append($method);
        }
        return $result;
    }

    /**
     * @param $cost
     * @param array $method
     * @return float|string
     */
    public function _getMethodPrice($cost, array $method, $subtotal)
    {
        return !empty($method['free_shipping_subtotal']) && $method['free_shipping_subtotal'] <= $subtotal
            ? '0.00'
            : $this->getFinalPriceWithHandlingFee($cost);
    }

    /**
     * @param $trackings
     * @return \Magento\Shipping\Model\Tracking\Result
     */
    public function getTracking($trackings)
    {
        $response = $this->_internetDocument->getTrackingInfo($trackings);

        return $this->_parseTrackingResponse($response);
    }

    protected function _parseTrackingResponse($response)
    {
        $result = $this->_trackFactory->create();
        if (isset($response['success']) && $response['success']) {
            $tracking = $this->_trackStatusFactory->create();
            $tracking->setCarrier($this->getCarrierCode());
            $tracking->setCarrierTitle($this->getConfigData('title'));
            foreach ($response['data'] as $data) {
                $tracking->setTracking($data['Number']);
                $tracking->setUrl('https://novaposhta.ua/tracking/?cargo_number='.$data['Number']);
                $tracking->setStatus($data['Status']);
                $tracking->setSignedby($data['RecipientFullName']);
            }

            $result->append($tracking);
        } else {
            $error = $this->_trackErrorFactory->create();
            $error->setCarrier('NovaPoshta');
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setTracking($response['Number']);
            $error->setErrorMessage(
                isset($response['error']) ? $response['error'] : __('For some reason we can\'t retrieve tracking info right now.')
            );
            $result->append($error);
        }
        return $result;
    }

    /**
     * @param string $data
     * @param string $methodCode
     * @return string
     */
    protected function getCacheKey(string $data, string $methodCode)
    {
        $key = self::CODE;
        $key .= '_' . $this->_rawRequest->getWeight();
        $key .= '_' . $this->_rawRequest->getCityRecipient();
        $key .= '_' . $this->_rawRequest->getCitySender();
        foreach ($this->_rawRequest->getVolumes() as $volume) {
            $key .= '_' . implode('_', $volume);
        }
        $key .= '_' . $methodCode;
        $key .= '_' . $data;
        return $key;
    }

    /**
     * @param RateRequest $request
     * @param array $customAttributes
     * @param string $methodCode
     * @param string $data
     * @return bool|string
     */
    private function _calcData(RateRequest $request, array $customAttributes, string $methodCode, string $data)
    {
        if (!isset($this->_apiMethods[$data]) || !method_exists($this->_internetDocument, $this->_apiMethods[$data])) {
            return false;
        }
        try {
            $this->_prepareRequest($request, $customAttributes, $methodCode);
            $cacheKey = $this->getCacheKey($data, $methodCode);
            $result = $this->_cache->load($cacheKey);
            if ($result !== false) {
                return $result;
            }
            $result = $this->_internetDocument->{$this->_apiMethods[$data]}($this->_rawRequest);
            $this->_cache->save($result, $cacheKey, [], self::CACHE_LIFETIME);
        } catch (\InvalidArgumentException $e) {
            $result = false;
        }
        return $result;
    }

    /**
     * @param RateRequest $request
     * @param $customAttributes
     * @param string $methodCode
     */
    private function _prepareRequest(RateRequest $request, array $customAttributes, string $methodCode)
    {
        if (isset($this->_rawRequest) && $this->_rawRequest->getServiceType() === $methodCode) {
            return;
        }

        if (!$request->getOrigCity()) {
            $request->setOrigCity($this->getConfigData('city_sender'));
        }

        if (!isset($customAttributes['city_id'])) {
            throw new \InvalidArgumentException(__('No city_id received yet.'));
        }

        $r = $this->_requestFactory->create();
        $r->setCityRecipient($customAttributes['city_id']);
        $r->setBaseSubtotalInclTax($request->getBaseSubtotalInclTax());
        $r->setWeight($request->getPackageWeight());
        $r->setCitySender($request->getOrigCity());
        $r->setDateTime($this->_getSendDate());
        $r->setServiceType($methodCode);
        $r->setVolumes($request->getVolumes());
        $r->setCargoType($this->getConfigData('cargo_type'));
        $this->setRawRequest($r);
    }

    /**
     * @return false|string
     */
    private function _getSendDate()
    {
        $day = (int)$this->getConfigData('shipping_date');
        return $this->_formatDate(strtotime("+$day days 2 hours"));
    }

    /**
     * @param $time
     * @return false|string
     */
    private function _formatDate($time)
    {
        return date($this->dateFormat, $time);
    }

    /**
     * @param array $data
     */
    private function _updateShippingData(array $data)
    {
        if ($this->_orderQuoteId) {
            $shippingData = $this->_shippingDataRepository->get($this->_orderQuoteId);
            $shippingData['tracking_ref'] = $data['Ref'];
            $this->_shippingDataRepository->save($this->_orderQuoteId, $shippingData);
        }
    }
}
