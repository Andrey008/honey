<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model;

use Mart\NovaPoshta\Api\InternetDocumentInterface;
use Mart\NovaPoshta\Helper\Config;
use Mart\NovaPoshta\Model\Counterparty\ContactPerson;
use Mart\NovaPoshta\Model\Gateway\NovaPoshtaApi2;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;

class InternetDocument extends AbstractApi implements InternetDocumentInterface
{
    /**
     * @var array
     */
    protected $dimensionCoefficient = [
        'CENTIMETER' => 0.01,
        'INCH' => 0.0254
    ];

    /**
     * @var array
     */
    protected $weightCoefficient = [
        'KILOGRAM' => 1,
        'POUND' => 0.3732417216
    ];

    /**
     * @var array
     */
    protected $warehouseSenderMethods = [
        'WarehouseDoors',
        'WarehouseWarehouse'
    ];

    /**
     * @var array
     */
    protected $addressRecipientMethods = [
        'DoorsDoors',
        'WarehouseDoors'
    ];

    /**
     * @inheritdoc
     */
    public function getDocumentPrice(\Mart\NovaPoshta\Model\Api\Request $data)
    {
        try {
            $result = $this->_api->getDocumentPrice(
                $data->getCitySender(),
                $data->getCityRecipient(),
                $data->getServiceType(),
                $data->getWeight(),
                $data->getBaseSubtotalInclTax(),
                $data->getVolumes(),
                $data->getCargoType()
            );
        } catch (LocalizedException $e) {
            throw new \InvalidArgumentException($e->getMessage());
        }

        $price = '';

        foreach ($result['data'] as $item) {
            $price = $item['Cost'] ?? '';
        }

        return $price;
    }

    /**
     * @inheritdoc
     */
    public function getDocumentDeliveryDate(\Mart\NovaPoshta\Model\Api\Request $data)
    {
        try {
            $result = $this->_api->getDocumentDeliveryDate(
                $data->getCitySender(),
                $data->getCityRecipient(),
                $data->getServiceType(),
                $data->getDateTime()
            );
        } catch (LocalizedException $e) {
            throw new \InvalidArgumentException($e->getMessage());
        }
        $date = '';
        foreach ($result['data'] as $item) {
            $date = $item['DeliveryDate']['date'] ?? '';
        }
        return $date;
    }

    /**
     * @param \Magento\Framework\DataObject|\Mart\NovaPoshta\Model\Api\Request $data
     * @return mixed
     */
    public function createDocument(\Magento\Framework\DataObject $data)
    {
        $packageParams = $data->getPackageParams();
        /** @var ContactPerson $senderContactPerson */
        $senderContactPerson = $data->getSender();
        $sender = [
            'Sender' => $senderContactPerson->getCounterpartyRef(),
            'Description' => $senderContactPerson->getFullName(),
            'ContactSender' => $senderContactPerson->getRef(),
            'SendersPhone' => $senderContactPerson->getPhones(),
            'CitySender' => $data->getShipperAddressCity(),
        ];
        if (in_array($data->getShippingMethod(), $this->warehouseSenderMethods)) {
            $sender['SenderAddress'] = $data->getShipperWarehouseRef();
        }
        $recipient = [
            'CityRecipient' => $data->getCityId(),
            'City' => $data->getCityId(),
            'CityRef' => $data->getCityId(),
            'RecipientAddress' => $data->getWarehouseId(),
            'RecipientsPhone' => $data->getRecipientContactPhoneNumber(),
            'FirstName' => $data->getRecipientContactPersonFirstName(),
            'LastName' => $data->getRecipientContactPersonLastName(),
            'Phone' => $data->getRecipientContactPhoneNumber(),
            'CounterpartyType' => 'PrivatePerson',
            'Recipient' => false,
        ];
        $params = [
            'PayerType' => $data->getPayerType(),
            'PaymentMethod' => $data->getPaymentMethod(),
            'CargoType' => $data->getCargoType(),
            'DateTime' => $data->getDateTime(),
            'VolumeGeneral' => $this->_calculateVolume($packageParams),
            'Weight' => $this->_calculateWeight($packageParams),
            'ServiceType' => $data->getShippingMethod(),
            'SeatsAmount' => '1',
        ];
        $params = array_merge($params, $this->_getItemParams($data));
        $this->logger->info(\Zend_Json::encode(array_merge($sender, $recipient, $params)));
        $result = $this->_api->newInternetDocument($sender, $recipient, $params);
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getTrackingInfo($tracking)
    {
        return $this->_api->documentsTracking($tracking);
    }

    /**
     * @inheritdoc
     */
    public function getDocumentList()
    {
        return $this->_api->getDocumentList();
    }

    /**
     * @param string $ref
     * @return mixed
     */
    public function delete($ref)
    {
        return $this->_api->deleteInternetDocument($ref);
    }

    /**
     * @param \Magento\Framework\DataObject $packageParams
     * @return float
     */
    protected function _calculateVolume(\Magento\Framework\DataObject $packageParams)
    {
        $coef = $this->dimensionCoefficient[$packageParams->getDimensionUnits()] ?? 1;
        $r = $packageParams->getLength() * $packageParams->getWidth() * $packageParams->getHeight() * pow($coef, 3);
        return round($r, 2);
    }

    /**
     * @param \Magento\Framework\DataObject $packageParams
     * @return float
     */
    protected function _calculateWeight(\Magento\Framework\DataObject $packageParams)
    {
        $coef = $this->weightCoefficient[$packageParams->getWeightUnits()] ?? 1;
        $r = $packageParams->getWeight() * $coef;
        return round($r, 2);
    }

    /**
     * @param \Magento\Framework\DataObject $data
     * @return array
     */
    protected function _getItemParams(\Magento\Framework\DataObject $data)
    {
        /** @var \Magento\Sales\Model\Order\Shipment $orderShipment */
        $orderShipment = $data->getOrderShipment();
        $result = [
            'Description' => '',
            'Cost' => 0,
        ];
        foreach ($orderShipment->getItems() as $item) {
            $result['Description'] .= $item->getName() . '.';
            $result['Cost'] += $item->getPrice();
        }
        $result['Description'] = substr($result['Description'], 0, 49);
        return $result;
    }
}
