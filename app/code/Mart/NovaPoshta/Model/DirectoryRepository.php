<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model;

use Mart\NovaPoshta\Api\DirectoryRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class DirectoryRepository extends AbstractRepository implements DirectoryRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function getAllCargoTypes()
    {
        return $this->_getFullCollection(ResourceModel\Directory\CargoType\Collection::class);
    }

    /**
     * @inheritdoc
     */
    public function getAllServiceTypes()
    {
        return $this->_getFullCollection(ResourceModel\Directory\ServiceType\Collection::class);
    }

    /**
     * @inheritdoc
     */
    public function getServiceType($ref)
    {
        /** @var \Mart\NovaPoshta\Model\Directory\ServiceType $model */
        $model = $this->_objectManager->create(\Mart\NovaPoshta\Model\Directory\ServiceType::class);
        $model->load($ref);
        if (!$model->getId()) {
            throw new NoSuchEntityException(__('No such service type with ref "%1"', $ref));
        }
        return $model;
    }

    /**
     * @param $class
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    private function _getFullCollection($class)
    {
        return $this->_getCollection($class)->load();
    }

    /**
     * @param $class
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    private function _getCollection($class)
    {
        $key = $class;
        if (!isset($this->_collections[$key])) {
            $this->_collections[$key] = $this->_objectManager->create($class);
        }
        return $this->_collections[$key];
    }
}
