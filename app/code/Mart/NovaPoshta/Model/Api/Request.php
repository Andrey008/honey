<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Api;

use Mart\NovaPoshta\Api\Data\RequestInterface;
use Magento\Framework\DataObject;

/**
 * Class Request
 * @package Mart\NovaPoshta\Model\Api
 * @method string getCityRecipient
 * @method setCityRecipient(string $data)
 * @method float getBaseSubtotalInclTax
 * @method setBaseSubtotalInclTax(float $data)
 * @method float getWeight
 * @method setWeight(float $data)
 * @method string getCitySender
 * @method setCitySender(string $data)
 * @method string getDateTime
 * @method setDateTime(string $data)
 * @method string getServiceType
 * @method setServiceType(string $data)
 * @method string getCargoType
 * @method setCargoType(string $data)
 * @method setVolumes(array $data)
 * @method array getVolumes
 */
class Request extends DataObject
{
}
