<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model;

use Mart\NovaPoshta\Api\AddressManagementInterface;
use Mart\NovaPoshta\Model\Address\Area;
use Mart\NovaPoshta\Model\Address\City;
use Mart\NovaPoshta\Model\Address\Warehouse;
use Mart\NovaPoshta\Model\Address\WarehouseType;

class AddressManagement extends AbstractManagement implements AddressManagementInterface
{
    /**
     * @inheritdoc
     */
    public function updateAll()
    {
        return [
            'Areas' => $this->updateAreas(),
            'Cities' => $this->updateCities(),
            'Warehouses' => $this->updateWarehouses(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function updateAreas()
    {
        $data = $this->_api->getAreas();
        $map = [
            'ref' => 'Ref',
            'center_ref' => 'AreasCenter',
            'description' => 'Description',
        ];
        return $this->_updateEntity(
            $data,
            Area::TABLE_NAME,
            $map,
            ['center_ref', 'description']
        );
    }

    /**
     * @inheritdoc
     */
    public function updateCities()
    {
        $map = [
            'ref' => 'Ref',
            'area_ref' => 'Area',
            'city_code' => 'CityID',
            'delivery_1' => 'Delivery1',
            'delivery_2' => 'Delivery2',
            'delivery_3' => 'Delivery3',
            'delivery_4' => 'Delivery4',
            'delivery_5' => 'Delivery5',
            'delivery_6' => 'Delivery6',
            'delivery_7' => 'Delivery7',
            'type' => 'SettlementTypeDescription',
            'type_ru' => 'SettlementTypeDescriptionRu',
            'description' => 'Description',
            'description_ru' => 'DescriptionRu',
            'is_branch' => 'IsBranch',
        ];
        $data = $this->_api->getCities();
        return $this->_updateEntity(
            $data,
            City::TABLE_NAME,
            $map,
            ['ref']
        );
    }

    /**
     * @inheritdoc
     */
    public function updateWarehouses()
    {
        $error = $this->_updateWarehouseTypes();
        if ($error) {
            return $error;
        }
        $data = $this->_api->getWarehouses();
        $map = [
            'ref' => 'Ref',
            'city_ref' => 'CityRef',
            'warehouse_key' => 'SiteKey',
            'description' => 'Description',
            'description_ru' => 'DescriptionRu',
            'short_address' => 'ShortAddress',
            'short_address_ru' => 'ShortAddressRu',
            'status' => function ($item) { return $item['WarehouseStatus'] == 'Working'; },
            'bicycle_parking' => 'BicycleParking',
            'type' => 'TypeOfWarehouse',
            'number' => 'Number',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'post_finance' => 'PostFinance',
            'pos_terminal' => 'POSTerminal',
            'international' => 'InternationalShipping',
            'max_weight' => 'TotalMaxWeightAllowed',
            'place_max_weight' => 'PlaceMaxWeightAllowed',
            'reception_schedule' => function ($item) { return \Zend_Json::encode($item['Reception']); },
            'delivery_schedule' => function ($item) { return \Zend_Json::encode($item['Delivery']); },
            'work_schedule' => function ($item) { return \Zend_Json::encode($item['Schedule']); },
            'site_key' => 'SiteKey',
        ];
        return $this->_updateEntity(
            $data,
            Warehouse::TABLE_NAME,
            $map,
            ['ref']
        );
    }

    /**
     * @return array
     */
    private function _updateWarehouseTypes()
    {
        $data = $this->_api->getWarehouseTypes();
        $map = [
            'ref' => 'Ref',
            'description' => 'Description',
        ];
        return $this->_updateEntity(
            $data,
            WarehouseType::TABLE_NAME,
            $map,
            ['ref']
        );
    }
}
