<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Address;

use Mart\NovaPoshta\Helper\Config;
use Magento\Framework\Model;

abstract class AbstractAddress extends Model\AbstractModel
{
    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * AbstractAddress constructor.
     *
     * @param Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param Config $configHelper
     * @param array $data
     */
    public function __construct(
        Model\Context $context,
        \Magento\Framework\Registry $registry,
        Config $configHelper,
        Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->configHelper = $configHelper;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        if ($this->configHelper->isShowRussian()) {
            return $this->_getData('description_ru');
        }
        return $this->_getData('description');
    }
}
