<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Address;

/**
 * Class Warehouse
 * @package Mart\NovaPoshta\Model\Address
 * @method string getRef
 * @method string getCityRef
 * @method string|null getWarehouseKey
 * @method string|null getType
 * @method string|null getNumber
 * @method int|null getStatus
 * @method int|null getBicycleParking
 * @method float|null getLongitude
 * @method float|null getLatitude
 * @method int|null getPostFinance
 * @method int|null getPosTerminal
 * @method int|null getInternational
 * @method int|null getMaxWeight
 * @method int|null getPlaceMaxWeight
 * @method int|null getSiteKey
 */
class Warehouse extends AbstractAddress
{
    const TABLE_NAME = 'np_warehouse';

    /**
     * Model constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init(\Mart\NovaPoshta\Model\ResourceModel\Address\Warehouse::class);
    }

    /**
     * @return string
     */
    public function getShortAddress()
    {
        if ($this->configHelper->isShowRussian()) {
            return $this->_getData('short_address_ru');
        }
        return $this->_getData('short_address');
    }

    /**
     * @return array
     */
    public function getReceptionSchedule() : array
    {
        return $this->getScheduleValue('reception_schedule');
    }

    /**
     * @return array
     */
    public function getDeliverySchedule() : array
    {
        return $this->getScheduleValue('delivery_schedule');
    }

    public function getWorkSchedule() : array
    {
        return $this->getScheduleValue('work_schedule');
    }

    /**
     * @param string $field
     * @return array
     */
    protected function getScheduleValue(string $field) : array
    {
        $value = $this->_getData($field);
        $result = [];
        try {
            $result = \Zend_Json::decode($value);
        } catch (\Zend_Json_Exception $e) {
            $this->_logger->error($e->getMessage());
        }
        return $result;

    }
}
