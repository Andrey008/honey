<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Address;

/**
 * Class Area
 * @package Mart\NovaPoshta\Model\Address
 * @method string getRef
 * @method string getCenterRef
 */
class Area extends AbstractAddress
{
    const TABLE_NAME = 'np_area';

    /**
     * Model constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init(\Mart\NovaPoshta\Model\ResourceModel\Address\Area::class);
    }

    public function getDescription()
    {
        return $this->_getData('description');
    }
}
