<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Address;

/**
 * Class City
 * @package Mart\NovaPoshta\Model\Address
 * @method string|null getRef
 * @method string|null getAreaRef
 * @method int|null getCityCode
 * @method int|null getIsBranch
 * @method int|null getDelivery1
 * @method int|null getDelivery2
 * @method int|null getDelivery3
 * @method int|null getDelivery4
 * @method int|null getDelivery5
 * @method int|null getDelivery6
 * @method int|null getDelivery7
 */
class City extends AbstractAddress
{
    const TABLE_NAME = 'np_city';

    /**
     * Model constructor
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init(\Mart\NovaPoshta\Model\ResourceModel\Address\City::class);
    }

    /**
     * @return string|null
     */
    public function getType()
    {
        if ($this->configHelper->isShowRussian()) {
            return $this->_getData('type_ru');
        }
        return $this->_getData('type');
    }
}
