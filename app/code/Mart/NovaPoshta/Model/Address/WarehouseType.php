<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model\Address;

/**
 * Class WarehouseType
 * @package Mart\NovaPoshta\Model\Address
 * @method string getRef
 * @method string getDescription
 */
class WarehouseType extends \Magento\Framework\Model\AbstractModel
{
    const TABLE_NAME = 'np_warehouse_type';
}
