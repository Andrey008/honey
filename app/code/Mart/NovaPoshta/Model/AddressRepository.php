<?php
/**
 * Copyright © Mart, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mart\NovaPoshta\Model;

use Mart\NovaPoshta\Api\AddressRepositoryInterface;
use Mart\NovaPoshta\Model\ResourceModel\Address\City\Collection as CityCollection;
use Mart\NovaPoshta\Model\ResourceModel\Address\Area\Collection as AreaCollection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;

class AddressRepository extends AbstractRepository implements AddressRepositoryInterface
{
    /**
     * @var \Mart\NovaPoshta\Model\ResourceModel\Address\Area\Collection|null
     */
    private $allAreasCollection;

    /**
     * @inheritdoc
     */
    public function getAllCities($withAreas = false)
    {
        /** @var CityCollection $collection */
        $collection = $this->_objectManager->create(CityCollection::class);
        if ($withAreas) {
            $collection->addAreaNames();
        }
        return $collection;
    }

    /**
     * @inheritdoc
     */
    public function getAllAreas()
    {
        if (!isset($this->allAreasCollection)) {
            $this->allAreasCollection = $this->_objectManager->create(AreaCollection::class);
        }
        return $this->allAreasCollection;
    }

    /**
     * @inheritdoc
     */
    public function getArea($ref)
    {
        return $this->_loadEntity(\Mart\NovaPoshta\Model\Address\Area::class, $ref);
    }

    /**
     * @inheritdoc
     */
    public function getCity($ref)
    {
        return $this->_loadEntity(\Mart\NovaPoshta\Model\Address\City::class, $ref);
    }

    /**
     * @inheritdoc
     */
    public function getWarehouse($ref)
    {
        return $this->_loadEntity(\Mart\NovaPoshta\Model\Address\Warehouse::class, $ref);
    }

    public function getWarehouseByCityAndNumber($cityRef, $number)
    {
        /** @var \Mart\NovaPoshta\Model\ResourceModel\Address\Warehouse $resource */
        $resource = $this->_objectManager->create(\Mart\NovaPoshta\Model\ResourceModel\Address\Warehouse::class);
        $entityId = $resource->getWarehouseIdByCityAndNumber($cityRef, $number);
        if (!$entityId) {
            throw new NoSuchEntityException(__('Warehouse number %1 does not exist in this city', $number));
        }
        $model = $this->_objectManager->create(\Mart\NovaPoshta\Model\Address\Warehouse::class);
        $model->load($entityId);
        return $model;
    }
}
